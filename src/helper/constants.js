export const SERVER_URL = 'http://backend.maxasl.com/public/api/v1/';

//Urls
export const VIDEO = 'video'; // for get all videos
export const FAVORITE = 'add-wish-list'; // for get all videos
export const PURCHASE = 'purchase-user'; // for get all videos
export const GET_ALL = 'get-all-video'; // for get all videos

//labels
export const CURRENT_USER = 'currentuser';

//store link
export const PLAY_STORE = "market://details?id=myandroidappid"
export const APP_STORE = "itms://itunes.apple.com/us/app/apple-store/myiosappid?mt=8"

//images
export const starbackground = require('../../assets/images/starbackground.png');
export const maxaslword = require('../../assets/images/maxaslword.png');
//export const appLogo = require('../../assets/images/appLogo.png');
export const about_btn = require('../../assets/images/btnAbout.png');
export const MaxASL_App = require('../../assets/images/maxasl.png');
export const img = require('../../assets/images/img.png');
export const arrowLeft = require('../../assets/images/arrowleft.png');
export const arrowRight = require('../../assets/images/arrowright.png');
export const inappBanner = require('../../assets/images/buybanners.png');
export const labelRead = require('../../assets/images/btnreadstories.png');
export const labelWatch = require('../../assets/images/btnwatchstory.png');
export const words = require('../../assets/images/words.png');
export const boy = require('../../assets/images/Boy.png');
export const girlbook = require('../../assets/images/Girl_Books.png');
export const download = require('../../assets/images/downloadnow.png');
export const selectedall = require('../../assets/images/selectedall.png');
export const downloadselected = require('../../assets/images/downloadselected.png');
export const starline = require('../../assets/images/starline.png');
export const btnBuy = require('../../assets/images/btnbuy.png');
export const watchmore = require('../../assets/images/watchmore.png');
export const subtitle = require('../../assets/images/subtitle.png');
export const subtitlefill = require('../../assets/images/closedcaption.png');
export const btnreplay = require('../../assets/images/btnreplay.png');
export const btnreplaylong = require('../../assets/images/btnreplaylong.png');
export const btnreadstory = require('../../assets/images/btnreadstory.png');
export const btnreadagain = require('../../assets/images/btnreadagain.png');
export const btnvocabulary = require('../../assets/images/btnvocabulary.png');
export const btnforward = require('../../assets/images/btnforward.png');
export const btnbackward = require('../../assets/images/btnbackward.png');
export const playround = require('../../assets/images/playround.png');
export const plane = require('../../assets/images/plane.png');
export const thumb = require('../../assets/images/thumb.png');
export const earth = require('../../assets/images/earth.png');
export const heart = require('../../assets/images/heart.png');
export const heartoutline = require('../../assets/images/heartoutline.png');
export const heartfill = require('../../assets/images/heartfill.png');
export const bottomfooter = require('../../assets/images/bottomfooter.png');
export const watchnow = require('../../assets/images/watchnow.png');
export const books = require('../../assets/images/books.png');
export const inapp = require('../../assets/images/inapp.png');

//colors
export const purple = '#5E368E';
