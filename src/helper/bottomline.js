import React, { Component } from 'react';
import { View } from 'react-native';



class Bottomline extends Component {

  render() {
    const { container } = styles;
    return (
      <View style={container}>
        {this.renderBox('#CB3D37')}
        {this.renderBox('#C7202D')}
        {this.renderBox('#EC813D')}
        {this.renderBox('#F7CD52')}
        {this.renderBox('#4AB1EB')}
        {this.renderBox('#3D7EC0')}
        {this.renderBox('#731A67')}
      </View>
    );
  }

  renderBox(color) {
    return(
      <View style={[styles.innerContainer, {backgroundColor: color}]} />
    )
  }

  renderLabel() {

  }
}

const styles = {
  container: {
    flexDirection: 'row',
  },
  innerContainer: {
    flex: 1,
    height:7
  }
};

export default Bottomline;
