import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Image } from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
import styles from '../../styles';
import {playround} from '../../helper/constants';


class SubStoryCard extends Component { 

  openVideo(voc, title) {
    this.props.showPreview("voc", voc, title, this.props.item.index, this.props.item.item.subtitle_file); 
  }  

  render() { 
    const {cardContainer, cardImage } = styles;
    const {tumb_images, video_url, video_title} = this.props.item.item;
    const {  btnPlay } = stylesInline;
    return (
        <View style={cardContainer} index={this.props.item.index}>
            <Text style={[{fontSize: 17, fontFamily: 'gilroy-extrabold', alignSelf:'center', marginTop: 5, color:'#1D358C'}]}>
                {video_title}
            </Text>
            <ImageBackground 
                resizeMode={ImageResizeMode.stretch}
                source={{ uri: tumb_images }}
                style={[cardImage, {height: 155, width: 165}]}>
            </ImageBackground>
            <View style={{flex:1, justifyContent:'center'}}>
                <TouchableOpacity style={btnPlay} onPress={() => this.openVideo(video_url, video_title)}>
                    <Image source={playround} resizeMode={ImageResizeMode.contain} style={{width:40, height:40}}/>
                </TouchableOpacity>             
            </View>
        </View>
    );
  }
}

const stylesInline = {
    lockImage: {
        fontSize: 50,
        color: "#FFFFFF"
    },
    circleLockView: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: '#00000090'
    },
    borders: {
        borderRadius: 50,
        borderWidth: 3,
        borderColor: '#FFF',
        padding: 8,
    },
    iconPlay: {
        color: '#FFF',
        fontSize: 18
    },
    btnPlay: {
        alignSelf: 'center',
        marginTop: -20,
    },
    txtWatch: {
        color: '#FFF',
        marginLeft: 5,
        alignSelf: 'center',
    },
    previewBtn: {
        flexDirection: 'row',
        backgroundColor: '#1D358C',
        padding: 8,
        borderColor: '#FFF',
        borderWidth: 3
    },
    btnPreview: {
        position: 'absolute',
        bottom: 5,
        left: 4,
    },
    btnHeart: {
        position: 'absolute',
        borderWidth:1,
        bottom: 8,
        right: 5,
        borderColor:'rgba(0,0,0,0.1)',
        alignItems:'center',
        justifyContent:'center',
        width:30,
        height:30,
        backgroundColor:'#ffffff99',
        borderRadius:30,
    }
}

export default SubStoryCard;
