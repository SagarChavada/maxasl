import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, ActivityIndicator, Image} from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
import DeviceInfo from 'react-native-device-info';
import {Icon} from 'native-base';
import styles from '../../styles';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import {FAVORITE, SERVER_URL, playround, heartfill, heartoutline} from '../constants';

class StoryCard extends Component {

    state = {
        hide: true
    }

  render() {
    const {hide} = this.state;  
    const { type, favourite, tumb_images, title, main_video, id, banner_images, sentance_video, word_video } = this.props.items;    
    const {cardMainView, cardTextTitle, cardImageView, cardImage, heartIcon} = styles;
    const { btnHeart, btnPlay } = stylesInline;
    return (
        <View style={[cardMainView, {height: this.props.height, width:this.props.width}]} >
            <Text style={[cardTextTitle, {fontSize: 18, fontFamily: 'gilroy-extrabold'}]} > 
                {title}
            </Text>

            <View style={cardImageView}>
            <TouchableOpacity  onPress={() => Actions.story({title: title, mainVideo: main_video, 
                readVideo: sentance_video, wordVideo: word_video, banner: banner_images})}>
                <ImageBackground 
                    resizeMode={ImageResizeMode.stretch}
                    source={{ uri: tumb_images }}
                    style={[cardImage, {height: this.props.imgHeight, width: this.props.innerWidth}]}>
                    {!type && this.renderLockImage()}

                </ImageBackground>
                </TouchableOpacity>

                <View style={{flex:1, justifyContent:'flex-end'}} >
                        <View style={{flexDirection:'row', justifyContent:'space-between', }} >
                        {
                            !type ? this.renderButton(main_video, title, "preview", this.props.items, this.props.left) : 
                            <TouchableOpacity onPress={() => this.openVideo(main_video, title, this.props.items)}>
                                <Image source={playround} resizeMode={ImageResizeMode.contain} style={btnPlay}/>  
                            </TouchableOpacity>               
                        }
                        {
                               type &&  <TouchableOpacity onPress={() => this.addTofavorite(id)} style={btnHeart}>
                                <Image 
                                    source={favourite ? heartfill : heartoutline} 
                                    resizeMode={ImageResizeMode.contain} 
                                    style={{ width: 30, height: 30}}
                                    />
                                </TouchableOpacity>
                        }
                        </View>
                </View>
                </View>
        </View>
    );
  }

  openVideo(mainVideo, title, items) {
    this.props.showPreview(mainVideo, title, "free", items)
  }

  renderSpinner() {
      return (
          <View style={stylesInline.circleLockView}>
              <ActivityIndicator size="large" color="#F7CD52" />
          </View>
      )
  }

  renderLockImage() {
        return (
            <TouchableOpacity style={stylesInline.circleLockView} onPress={() => Actions.banner()}>
                    <View style={stylesInline.borders}>
                        <Icon name="lock" type="EvilIcons" style={stylesInline.lockImage} />
                    </View>
            </TouchableOpacity>
        )
    }

   renderButton(video, title, type, item, left) {
        return(
            <TouchableOpacity style={[stylesInline.btnPreview, { left: left}]} onPress={() => this.props.showPreview(video, title, type, item)}>
                <View style={stylesInline.previewBtn}>
                    <Icon name='play' style={stylesInline.iconPlay} type="FontAwesome" />
                    <Text style={stylesInline.txtWatch}>Watch Preview!</Text>
                </View>
            </TouchableOpacity>
        )
    }

    addTofavorite(videoId) {
        const favoritedata = `device_id=${DeviceInfo.getUniqueID()}&story_id=${videoId}`;
        axios.post(SERVER_URL + FAVORITE, favoritedata)
        .then((response) => {
            const status = response.data.status;
            const title = status == 0 ? "Remove from Favorite" : "Added To Favorite"
            this.props.showToast(title);
        })
        .catch((error) => {
            console.log(`Error: ${error}`);
        });
    }
}

const stylesInline = {
    lockImage: {
        fontSize: 30,
        color: '#FFF'
    },
    btnPlay: {
        position: 'absolute',
        bottom: 5,
        left: 3,
        width:40,
        height:40,
    },
    btnHeart: {
        position: 'absolute',
        bottom: 8,
        right: 0,   
    },
    txtWatch: {
        color: '#FFF',
        marginLeft: 5,
        alignSelf: 'center',
        fontFamily: 'gilroy-extrabold'
    },
    previewBtn: {
        borderTopRightRadius: 5,
        flexDirection: 'row',
        backgroundColor: '#1D358C',
        padding: 8,
        borderColor: '#FFF',
        borderWidth: 3,

    },
    btnPreview: {
        position: 'absolute',
        bottom: 5,
    },
    lockImage: {
        fontSize: 50,
        color: "#FFFFFF"
    },
    circleLockView: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: '#00000090'
    },
    borders: {
        borderRadius: 50,
        borderWidth: 3,
        borderColor: '#FFF',
        padding: 8,
    },
    iconPlay: {
        color: '#FFF',
        fontSize: 18
    },
}

export default StoryCard;
