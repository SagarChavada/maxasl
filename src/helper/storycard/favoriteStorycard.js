import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, ActivityIndicator, Image } from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
import DeviceInfo from 'react-native-device-info';
import styles from '../../styles';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import { FAVORITE, SERVER_URL, playround, heartfill, heartoutline } from '../constants';

class StoryCard extends Component {

    render() {
        const { favourite, tumb_images, title, id, video_details, banner_image } = this.props.items;
        const { cardMainView, cardTextTitle, cardImageView, cardImage } = styles;
        const { btnHeart } = stylesInline;
        return (
            <View>
                { 
                    favourite && 
                    <View style={[cardMainView, { height: this.props.height, width: this.props.width }]} >
                        <Text style={[cardTextTitle, { fontSize: 18, fontFamily: 'gilroy-extrabold' }]} >
                            {title}
                        </Text>
        
                        <View style={cardImageView}>
                            <TouchableOpacity onPress={() => Actions.story({ title: title, data: video_details, banner: banner_image })}>
                                <ImageBackground
                                    resizeMode={ImageResizeMode.stretch}
                                    source={{ uri: tumb_images }}
                                    style={[cardImage, { height: this.props.imgHeight, width: this.props.innerWidth }]}>
                                    
                                </ImageBackground>
                            </TouchableOpacity>
        
                            <View style={{ flex: 1, justifyContent: 'flex-end' }} >
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }} >
                                    <View></View>
                                    <TouchableOpacity onPress={() => this.addTofavorite(id)}
                                        style={btnHeart}>
                                        <Image
                                            source={favourite ? heartfill : heartoutline}
                                            resizeMode={ImageResizeMode.contain}
                                            style={{ width: 30, height: 30 }}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                }
            </View>
            
        );
    }

    renderSpinner() {
        return (
            <View style={stylesInline.circleLockView}>
                <ActivityIndicator size="large" color="#F7CD52" />
            </View>
        )
    }

    addTofavorite(videoId) {
        const favoritedata = `device_id=${DeviceInfo.getUniqueID()}&story_id=${videoId}`;
        axios.post(SERVER_URL + FAVORITE, favoritedata)
            .then((response) => {
                const status = response.data.status;
                const title = status == 0 ? "Remove from Favorite" : "Added To Favorite"
                this.props.showToast(title);
            })
            .catch((error) => {
                console.log(`Error: ${error}`);
            });
    }
}

const stylesInline = {
    lockImage: {
        fontSize: 30,
        color: '#FFF'
    },
    btnPlay: {
        position: 'absolute',
        bottom: 5,
        left: 3,
        width: 40,
        height: 40,
    },
    btnHeart: {
        position: 'absolute',
        bottom: 8,
        right: 0,
    },
    txtWatch: {
        color: '#FFF',
        marginLeft: 5,
        alignSelf: 'center',
    },
    previewBtn: {
        borderTopRightRadius: 5,
        flexDirection: 'row',
        backgroundColor: '#1D358C',
        padding: 8,
        borderColor: '#FFF',
        borderWidth: 3
    },
    btnPreview: {
        position: 'absolute',
        bottom: 5,
        left: 4,
    },
    lockImage: {
        fontSize: 50,
        color: "#FFFFFF"
    },
    circleLockView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00000090'
    },
    borders: {
        borderRadius: 50,
        borderWidth: 3,
        borderColor: '#FFF',
        padding: 8,
    },
    iconPlay: {
        color: '#FFF',
        fontSize: 18
    },
}

export default StoryCard;
