import React, {Component} from 'react';
import {Dimention, Platform} from 'react-native';

export class dimentionHelper extends Component{

    constructor() {
        super();
        this.state = { orientation: '' }
    }

    componentDidMount()
    {
        this.getOrientation();
        
        Dimensions.addEventListener( 'change', () => {
            this.getOrientation();
        });
    }

    getOrientation = () =>
    {
        if( this.refs.rootView )
        {
            if( Dimensions.get('window').width < Dimensions.get('window').height )
            {
            this.setState({ orientation: 'portrait' });
            }
            else
            {
            this.setState({ orientation: 'landscape' });
            }
        }
    }
}