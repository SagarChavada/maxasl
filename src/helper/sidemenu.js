import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Platform, Linking } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';
import { APP_STORE, PLAY_STORE } from './constants';

class SideMenu extends Component {

 render() {
   const { container } = styles;
   return (
     <View style={container}>
       {this.renderBox('#3FA257','Watch Offline','download','Feather', 'offline', "#FFF" )}
       {this.renderBox('#5E368E','Buy the Books','book-open','SimpleLineIcons', 'buybook', '#F4F2F4')}
       {this.renderBox('#3B99E3','Settings','settings','MaterialCommunityIcons', 'setting', "#FFF")}
       {this.renderLabel1('#B0071F','Love this? Write a review!','heart','Entypo', '#F4F2F4')}
     </View>
   );
 }

 navigateToScreen(type) {
   if(type == 'setting') {
     Actions.setting();
   } else if( type == "offline") {
     Actions.offline();
   } else {
     
   }
 }

 renderBox(color, title, icon, type, screen, background) {
   return (
     <TouchableOpacity style={[styles.innerContainer, {backgroundColor: background}]} onPress={() => this.navigateToScreen(screen)}>
      <Icon name={icon} type={type} style={{color:color}} />
      <Text style={{color:color, fontSize:20, fontFamily: 'gilroy-extrabold',}}>{title}</Text>
     </TouchableOpacity>
   )
 }

 renderLabel(color,title,icon,type, background) {
   return(
   <TouchableOpacity style={[styles.innerlabel, {backgroundColor: background}]}>
      <Icon name={icon} type={type} style={{color:color, fontSize: 18 }} />
      <Text style={[styles.txtLabel, {color: color}]}>{title}</Text>
   </TouchableOpacity>
   )
 }

  renderLabel1(color,title,icon,type, background) {
    return(
      <TouchableOpacity style={[styles.innerlabel, {backgroundColor: background}]} onPress={() => this.handleReview()}>
        <Icon name={icon} type={type} style={{color:color, fontSize: 18 }} />
        <Text style={[styles.txtLabel1, {color: color}]}>{title}</Text>
      </TouchableOpacity>
    )
  }

  handleReview() {
    if(Platform.OS =='ios'){
      Linking.openURL(APP_STORE).catch(err => console.error('An error occurred', err));
    }
    else{
        Linking.openURL(PLAY_STORE).catch(err => console.error('An error occurred', err));
    }
  }
}

const styles = {
 container: {
   flex: 1,
 },
 innerContainer: {
   flex: 1,
   justifyContent:'center',
   alignItems:'center'
 },
 innerlabel:{
   flexDirection:'row',
   justifyContent: 'center',
   alignItems: 'center',
   height:40  
  },
  txtLabel: {
    fontFamily: 'gilroy-extrabold',
    fontSize:18, 
    marginLeft: 10
  },
  txtLabel1: {
    fontSize:15, 
    marginLeft: 10,
    fontWeight: '400',
  }
};

export default SideMenu;