import React from 'react';
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Icon } from 'native-base';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
import {purple, maxaslword} from './constants';
import { Actions } from 'react-native-router-flux';

handleViewRef = ref => this.view = ref;

const Appbar = (props) => {
    const { container} = styles;
    const { iconLeft, backTitle } = styles;
    return (
        <View style={[container]}>
            <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => Actions.pop()}>
            <Icon name='arrow-left' type='Entypo' style={iconLeft} />
            <Text style={backTitle}>All Stories</Text>
            </TouchableOpacity>
        </View>
    )
}

renderBackIcon = () => {
    const { iconLeft, backTitle } = styles;
    return(
        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => Actions.pop()}>
            <Icon name='arrow-left' type='Entypo' style={iconLeft} />
            <Text style={backTitle}>All Stories</Text>
        </TouchableOpacity>
    )
}

renderMenuIcon = (props) => {
    const { iconLeft } = styles;
    return(
        <Icon name='menu' type='MaterialIcons' style={iconLeft} onPress={() => props.navigation.toggleDrawer()}/>
    )
}

bounce = () => this.view.tada(800).then(endState => {});

renderImage = () => {
    return(
        <TouchableWithoutFeedback onPress={this.bounce}>
            <Animatable.View ref={this.handleViewRef}>
                <Image source={maxaslword}  style={styles.imgTitle} resizeMode={ImageResizeMode.center} />
            </Animatable.View>
        </TouchableWithoutFeedback>
    )
}

renderTitle  = (props) => {
    return <Text style={[styles.txtTitle, {color: '#56B1E9', paddingRight: 20}]}>{props.title}</Text>
}


const styles = {
    container: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        height: 50,
    },
    iconLeft: {
        marginLeft: 30,
        alignSelf: 'center',
        color: '#FFF'
    },
    iconRight: {
        marginRight: 30,
        alignSelf: 'center',
        color: purple,
        marginTop: 5,
    },
    txtTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 5,
        alignSelf: 'center',
    },
    backTitle: {
      fontSize: 18,
      fontWeight: 'bold',
      marginLeft: 10,  
      color: '#FFF',
      alignSelf: 'center',
    },
    imgTitle: {
        height: 50,
    },
    borderbottom: {
        borderBottomColor: '#EEE',
        borderBottomWidth: 1
    }
}

export default Appbar