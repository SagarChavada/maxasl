import React from 'react';
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback, Platform } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Icon } from 'native-base';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
import {purple, maxaslword} from './constants';
import { Actions } from 'react-native-router-flux';

handleViewRef = ref => this.view = ref;

const Appbar = (props) => {
    const { container, iconRight, borderbottom } = styles;
    return (
        <View style={[container, borderbottom]}>
            {props.navigation ? renderMenuIcon(props) : renderBackIcon()}
            {props.icon ? renderImage() : renderTitle(props)}
            {!props.isFav && <Icon name='heart' type='Foundation' style={iconRight} onPress={() => Actions.favorite({stories: props.stories})}/>}
        </View>
    )
}

renderBackIcon = () => {
    const { iconLeft, backTitle } = styles;
    return(
        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => Actions.pop()}>
            <Icon name='arrow-left' type='Entypo' style={iconLeft} />
            <Text style={backTitle}>All Stories</Text>
        </TouchableOpacity>
    )
}

renderMenuIcon = (props) => {
    const { iconLeft } = styles;
    return(
        <Icon name='menu' type='Entypo' style={iconLeft} onPress={() => props.navigation.toggleDrawer()}/>
    )
}

bounce = () => this.view.tada(800).then(endState => {});

renderImage = () => {
    return(
        <TouchableWithoutFeedback onPress={this.bounce}>
            <Animatable.View ref={this.handleViewRef}>
                <Image source={maxaslword}  style={styles.imgTitle} resizeMode={ImageResizeMode.contain} />
            </Animatable.View>
        </TouchableWithoutFeedback>
    )
}

renderTitle  = (props) => {
    return <Text style={[styles.txtTitle, {color: props.color, paddingRight: 20}]}>{props.title}</Text>
}


const styles = {
    container: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        height: ( Platform.OS === 'ios' ) ? 60 : 50,
        backgroundColor: '#FFF',
        paddingTop: ( Platform.OS === 'ios' ) ? 10 : 0
    },
    iconLeft: {
        marginTop: 5,
        marginLeft: 20,
        alignSelf: 'center',
        color: purple
    },
    iconRight: {
        marginRight: 20,
        alignSelf: 'center',
        color: purple,
        marginTop: 10,
    },
    txtTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 5,
        alignSelf: 'center',
        marginRight: 65,
    },
    backTitle: {
      fontSize: 18,
      fontWeight: 'bold',
      color: purple,
      alignSelf: 'center',
    },
    imgTitle: {
        height: 40,
        marginTop: 5,
    },
    borderbottom: {
        borderBottomColor: '#EEE',
        borderBottomWidth: 1
    }
}

export default Appbar