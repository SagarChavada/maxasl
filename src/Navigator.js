import React from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';

// Pages
import NavigationView from './screens/dashboard'
import Setting from './drawer-screens/setting';
import Offline from './drawer-screens/offline';
import Banner from './screens/banner';
import Story from './screens/story';
import Favorite from './screens/favorite';

import ReadStory from './drawer-screens/offline/readStory';
import WatchStory from './drawer-screens/offline/watchstory';
import VocabularyWords from './drawer-screens/offline/wordStory';

const Navigator = () => (
    <Router>
        <Stack key="root">
          <Scene key="navigation" component={NavigationView} hideNavBar initial />
          <Scene key="setting" component={Setting} hideNavBar />
          <Scene key="offline" component={Offline} hideNavBar />
          <Scene key="banner" component={Banner} hideNavBar />
          <Scene key="story" component={Story} hideNavBar />
          <Scene key="readStory" component={ReadStory} hideNavBar />
          <Scene key="watchStory" component={WatchStory} hideNavBar />
          <Scene key="wordStory" component={VocabularyWords} hideNavBar />
          <Scene key="favorite" component={Favorite} hideNavBar />
        </Stack>
    </Router>
)

export default Navigator;