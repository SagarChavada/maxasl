import React, { Component } from 'react'
import Video from 'react-native-video';

import Slider from 'react-native-slider';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';

import { View, Dimensions, Platform, TouchableOpacity } from 'react-native';
const width = Dimensions.get('window').width;
const height =  Dimensions.get('window').height;

export default class AndroidVideo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			toggleVideo: false,
			videoMaxTime: 0,
			progressVideoValue: 0,
			progressSliderValue: 0,
			hideControls: false,
			fullScreen: false,
			isVisible: false,
			loaderShow: false,
			isImage: true,
			rate: 1,
			volume: 1,
			muted: false,
			resizeMode: 'contain',
			duration: 0.0,
			currentTime: 0.0,
			controls: false,
			paused: true,
			skin: 'custom',
			ignoreSilentSwitch: null,
			isBuffering: false,
			videoMaxTime: 0,
			progressVideoValue: 0,
			progressSliderValue: 0,
			fullScreen: false
		}
		this.onLoad = this.onLoad.bind(this);
		this.onProgress = this.onProgress.bind(this);
		this.onBuffer = this.onBuffer.bind(this);

	}
	hideControls() {
		setTimeout(() => {
			this.setState({ hideControls: true })
		}, 3000)
	}


	onLoad(data) {
		this.setState({ duration: data.duration });
	}

	onProgress(data) {
		if (this.state.videoMaxTime != data.seekableDuration) {
			this.setState({
				videoMaxTime: data.seekableDuration
			})
			if (data.currentTime === data.duration) {
				this.onEnd();
			}
		}
		this.setState({
			progressVideoValue: data.currentTime,
			progressSliderValue: data.currentTime
		})
	}
	onBuffer({ isBuffering }) {
		this.setState({ isBuffering });
	}
	render() {
		return (
			<View style={{ flex: 1 }} >
				<TouchableOpacity onPress={() => { this.setState({ hideControls: !this.state.hideControls }); this.hideControls(); }}>
					<Video source={{ uri: this.props.previewUrl }}   // Can be a URL or a local file.
						ref={(ref) => {
							this.video = ref
						}}

						// textTracks={sub_title}
						rate={this.state.rate}
						paused={this.state.paused}
						volume={this.state.volume}
						muted={this.state.muted}
						ignoreSilentSwitch={this.state.ignoreSilentSwitch}
						resizeMode={this.state.resizeMode}
						onLoad={this.onLoad}
						onBuffer={this.onBuffer}
						onProgress={this.onProgress}
						onEnd={() => { Platform.OS == 'ios' ? AlertIOS.alert('Done!') : this.onEnd }}
						style={{ width: width, height: this.state.fullScreen ? height : height - 50 }} />
				</TouchableOpacity>
				{!this.state.hideControls &&
					<View style={{ position: 'absolute', width: width, height: height, marginTop: 30 }}>
						<View style={{ height: height - 150 }}>
							<View style={{ backgroundColor: 'rgba(0,0,0,0.5)', width: 30, height: 30, marginHorizontal: 20, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
								<TouchableOpacity
									onPress={() => { this.props.fullScreen(!this.state.fullScreen); this.setState({ fullScreen: !this.state.fullScreen }) }}
									style={{ width: 50, height: 30, justifyContent: 'center', alignItems: 'center' }}
								>
									{
										!this.state.fullScreen ?
											<Entypo name='resize-full-screen' size={20} color={'#fff'} />
											:
											<Entypo name='resize-100-' size={20} color={'#fff'} />
									}
								</TouchableOpacity>
							</View>
						</View>

						{!this.state.fullScreen ?
							<View style={{ flexDirection: 'row', backgroundColor: 'rgba(0,0,0,0.5)', height: 30, marginHorizontal: 20, borderRadius: 10}}>
								<TouchableOpacity
									onPress={() => { this.setState({ paused: !this.state.paused }) }}
									style={{ width: 50, height: 30, justifyContent: 'center', alignItems: 'center' }}
								>
									{
										this.state.paused ?
											<AntDesign name='play' size={20} color={'#fff'} />
											:
											<AntDesign name='pause' size={20} color={'#fff'} />

									}
								</TouchableOpacity>
								<View
									style={{ width: width - 105, height: 30, justifyContent: 'center' }}
								>
									<Slider
										value={this.state.progressSliderValue}
										//  onValueChange={(data) => this.jumpToVideoTime(data)}
										maximumTrackTintColor='#c9c5bf'
										minimumTrackTintColor='#e11a21'
										thumbTintColor='#e11a21'
										maximumValue={this.state.videoMaxTime}
										thumbStyle={{ height: 12, width: 12 }}
									/>
								</View>
							</View> 
							: 
							<View style={{ flexDirection: 'row', backgroundColor: 'rgba(0,0,0,0.5)', height: 30, marginHorizontal: 20, borderRadius: 10, marginTop: 50 }}>
								<TouchableOpacity
									onPress={() => { this.setState({ paused: !this.state.paused }) }}
									style={{ width: 50, height: 30, justifyContent: 'center', alignItems: 'center' }}
								>
									{
										this.state.paused ?
											<AntDesign name='play' size={20} color={'#fff'} />
											:
											<AntDesign name='pause' size={20} color={'#fff'} />

									}
								</TouchableOpacity>
								<View
									style={{ width: width - 105, height: 30, justifyContent: 'center' }}
								>
									<Slider
										value={this.state.progressSliderValue}
										//  onValueChange={(data) => this.jumpToVideoTime(data)}
										maximumTrackTintColor='#c9c5bf'
										minimumTrackTintColor='#e11a21'
										thumbTintColor='#e11a21'
										maximumValue={this.state.videoMaxTime}
										thumbStyle={{ height: 12, width: 12 }}
									/>
								</View>
							</View>
						}
					</View>
				}
			</View>
		)
	}

}
