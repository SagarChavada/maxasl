import React, { Component } from 'react'
import {
	View, ImageBackground, Dimensions, ScrollView, StatusBar,
	TouchableOpacity, Image, Modal, BackHandler, Platform, Text, ToastAndroid
} from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode';
import Toast from 'react-native-toast-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
	// import Video from 'react-native-video';
import { connect } from 'react-redux';
import styles from '../styles';
import { starbackground, purple, arrowLeft, arrowRight } from '../helper/constants';
import Appbar from '../helper/appbar';
import StoryCard from '../helper/storycard/storycard';
import { fetchCategory } from '../action';
import BottomLine from '../helper/bottomline';
import Video from 'react-native-af-video-player';
import AndroidVideo from './androidVideo';

const { height, width } = Dimensions.get('window');
const aspectRatio = height / width;

const style = {
	backgroundColor: (Platform.OS === 'ios') ? '#000000' : "#EEEEEE",
	width: 300,
	height: 50,
	fontSize: 18,
	borderRadius: 15,
};

class Dashboard extends Component {

	constructor() {
		super();
		this.onNextClick = this.onNextClick.bind(this)
		this.onPreviosClick = this.onPreviosClick.bind(this)
		this.renderCardlistIpadSmall = this.renderCardlistIpadSmall.bind(this);
		this.renderCardlistIpadBig = this.renderCardlistIpadBig.bind(this);
		this.showToast = this.showToast.bind(this);
		this.showPreview = this.showPreview.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.onProgress = this.onProgress.bind(this);
		this.onBuffer = this.onBuffer.bind(this);
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		this.showPreviewVideo = this.showPreviewVideo.bind(this);
	}

	state = {
		orientation: '',
		stories: [],
		count: 0,
		isScroll: false,
		previewUrl: '',
		storyTitle: '',
		isBuffering: false,
		duration: 0.0,
		currentTime: 0.0,
		isBuffering: false,
		paused: false,
		isModelVideo: false,
		isSub: false,
		type: '',
		title: '',
		mainVideo: '',
		readVideo: '',
		wordVideo: '',
		banner: '',
		fullScreen:false
	}

	componentWillMount() {
		BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
	}

	componentDidMount() {
		this.getOrientation();

		Dimensions.addEventListener('change', () => {
			this.getOrientation();
		});
	}

	componentDidUpdate(prevProps) {
		if (prevProps.stories != this.props.stories) {
			this.setState({ stories: this.props.stories.data });
		}
	}

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
	}

	handleBackButtonClick() {
		if (this.state.isModelVideo) {
			this.setState({ isModelVideo: false, paused: true, isReadStory: false });
			if (Platform.OS == "ios") {
				this.player.seek(0);
			} else {
				// this.player.player.ref.seek(0);
			}
		} else {
			BackHandler.exitApp();
		}
	}

	onNextClick() {
		this.setState({ count: this.state.count - 1 }, () => {
			this.scroll.scrollTo({ x: this.state.count * 190, y: 0, animated: true })
		}
		);
	}

	onPreviosClick() {
		this.setState({ count: this.state.count + 1 }, () => {
			this.scroll.scrollTo({ x: this.state.count * 210, y: 0, animated: true })
		});
	}

	showToast(title) {
		if (Platform.OS == "ios") {
			Toast.show(title, Toast.SHORT, Toast.TOP, style);
		} else {
			ToastAndroid.showWithGravity(
				title,
				ToastAndroid.SHORT,
				ToastAndroid.CENTER,
			);
		}
		this.props.fetchCategory();
	}

	showPreview(previewUrl, storyTitle, type, item) {
		this.setState({
			previewUrl, storyTitle, isModelVideo: true, type, title: item.title, mainVideo: item.mainVideo,
			readVideo: item.sentance_video, wordVideo: item.word_video, banner: item.banner_images
		});
	}


	onLoad(data) {
		this.setState({ duration: data.duration });
	}

	onProgress(data) {
		this.setState({ currentTime: data.currentTime });
		if (data.currentTime === data.duration) {
			this.onEnd();
		}
	}

	_onProgress(data) {
		if (this.state.videoMaxTime != data.duration) {
			this.setState({ videoMaxTime: data.duration })
		}
		this.setState({
			progressVideoValue: data.currentTime + 0.5,
			progressSliderValue: data.currentTime + 0.5
		})
	}

	onBuffer({ isBuffering }) {
		this.setState({ isBuffering });
	}

	onEnd = () => {
		const { title, mainVideo, readVideo, wordVideo, banner } = this.state;
		if (Platform.OS == "ios") {
			this.player.seek(0);
		} else {
			this.player.player.ref.seek(0);
		}
		if (this.state.type == "free") {
			this.setState({ paused: true, isModelVideo: false });
			Actions.story({
				title: title, mainVideo: mainVideo,
				readVideo: readVideo, wordVideo: wordVideo, banner: banner
			})
		} else {
			this.setState({ paused: true, isModelVideo: false });
		}

	};

	onAudioBecomingNoisy = () => {
		this.setState({ paused: true });
	};

	onAudioFocusChanged = (event) => {
		this.setState({ paused: !event.hasAudioFocus });
	};


	showPreviewVideo() {
		const { previewUrl, paused } = this.state;
		const { container } = styleInline;
		return (
			<Modal animationType="slide"
				visible={this.state.isModelVideo}
				supportedOrientations={['landscape']}
				onRequestClose={() => { this.handleBackButtonClick() }}>
				<View style={{ flex: 1 }}>
					{!this.state.fullScreen &&
						<View>
							<View style={container}>
								{this.renderBackIcon()}
								<View style={{ flex: 1 }}>
									{this.renderTitle()}
								</View>

							</View>
						</View>
					}
					{
						Platform.OS == "ios" ?
							<Video source={{ uri: previewUrl }}
								ref={(ref) => {
									this.player = ref
								}}
								paused={paused}
								onLoad={this.onLoad}
								onBuffer={this.onBuffer}
								onProgress={this.onProgress}
								onEnd={this.onEnd}
								controls
								onAudioBecomingNoisy={this.onAudioBecomingNoisy}
								onAudioFocusChanged={this.onAudioFocusChanged}
								style={styles.backgroundVideo}>
							</Video> :
							<AndroidVideo
								previewUrl={previewUrl}
								fullScreen={val=>{this.setState({fullScreen:val})}}
							/>
					}
				</View>
			</Modal>
		)
	}

	renderBackIcon() {
		const { iconLeft } = styleInline;
		return (
			<TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.handleBackButtonClick()}>
				<Icon name='chevron-thin-left' type='Entypo' style={iconLeft} />
			</TouchableOpacity>
		)
	}

	renderTitle() {
		return <Text style={[styleInline.txtTitle]}>{this.state.storyTitle}</Text>
	}

	videoError = (error) => {
		// console.log("Video Error >>",error);
	}

	getOrientation = () => {
		if (this.refs.rootView) {
			if (Dimensions.get('window').width < Dimensions.get('window').height) {
				this.setState({ orientation: 'portrait' });
			}
			else {
				this.setState({ orientation: 'landscape' });
			}
		}
	}


	render() {
		const { container, imgBackground, containerPortrait } = styles;

		return (
			<View style={[(this.state.orientation == 'portrait') ? containerPortrait : container, { backgroundColor: purple }]} ref="rootView">
				<StatusBar
					backgroundColor="white"
					barStyle="dark-content"
				/>
				<Appbar navigation={this.props.navigation} icon={true} stories={this.state.stories} />
				<ImageBackground source={starbackground} style={imgBackground} resizeMode={ImageResizeMode.contain}>

					<ScrollView ref={re => this.scroll = re} horizontal={true} showsHorizontalScrollIndicator={false}>
						<View style={{ flex: 1, justifyContent: 'flex-end', marginLeft: 15 }}>
							{aspectRatio > 1.6 ?
								this.renderCardlistIphone() :
								aspectRatio > 0.66 ? this.renderCardlistIpadSmall() : this.renderCardlistIpadBig()}
						</View>
					</ScrollView>
				</ImageBackground>
				<BottomLine />
				{this.state.stories.length > 3 && this.renderLeftArrow()}
				{this.state.stories.length > 3 && this.renderRightArrow()}
				<View>
					{this.showPreviewVideo()}
				</View>
			</View>
		)
	};

	renderCardlistIphone() {
		const { stories, count } = this.state;
		return (
			<View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
				{
					stories.map((item, index) => {
						return index == count ? <StoryCard showToast={this.showToast} showPreview={this.showPreview} height={260} items={item} key={index} imgHeight={190} width={210} innerWidth={190} />
							: <StoryCard showToast={this.showToast} showPreview={this.showPreview} height={240} key={index} items={item} imgHeight={170} width={210} innerWidth={180} />
					})
				}
			</View>
		)
	}

	renderLeftArrow() {
		return (
			<View style={styles.footerLeftErroView} >
				<TouchableOpacity onPress={() => this.state.count != 0 && this.onNextClick()}>
					<Image style={styles.footerErroImage} source={arrowLeft} />
				</TouchableOpacity>
			</View>
		)
	}

	renderRightArrow() {
		return (
			<View style={styles.footerRightErroView}>
				<TouchableOpacity onPress={() => this.state.count != this.state.stories.length - 1 && this.onPreviosClick()}>
					<Image style={styles.footerErroImage} source={arrowRight} />
				</TouchableOpacity>
			</View>
		)
	}

	renderCardlistIpadSmall() {
		const { stories, count } = this.state;
		return (
			<View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
				{
					stories.map((item, index) => {
						return index == count ? <StoryCard showToast={this.showToast} showPreview={this.showPreview} items={item} height={220} key={index} imgHeight={150} left={30} width={200} innerWidth={190} />
							: <StoryCard showToast={this.showToast} showPreview={this.showPreview} items={item} height={200} key={index} imgHeight={130} width={200} innerWidth={180} left={30} />
					})
				}
			</View>
		)
	}

	renderCardlistIpadBig() {
		const { stories, count } = this.state;
		return (
			<View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
				{
					stories.map((item, index) => {
						return index == count ? <StoryCard showToast={this.showToast} showPreview={this.showPreview} height={270} items={item} key={index} imgHeight={200} width={210} left={37} innerWidth={190} />
							: <StoryCard showToast={this.showToast} showPreview={this.showPreview} height={250} key={index} imgHeight={180} items={item} width={200} innerWidth={180} left={30} />
					})
				}
			</View>
		)
	}
}

const styleInline = {
	container: {
		flexDirection: 'row',
		height: (Platform.OS === 'ios') ? 60 : 50,
		backgroundColor: purple,
		paddingTop: (Platform.OS === 'ios') ? 10 : 0
	},
	iconLeft: {
		color: "#FFF",
		marginTop: 10,
		marginLeft: 10,
	},
	iconRight: {
		marginRight: 30,
		marginTop: 10,
		height: 35,
		width: 35
	},
	txtTitle: {
		fontSize: 20,
		fontWeight: 'bold',
		marginTop: 15,
		color: '#FFF',
		alignSelf: 'center',
	},
	imgbtnreplay: {
		height: 150,
		width: 150,
		top: 120,
		left: '25%',
		position: 'absolute'
	},
	imgbtnreadstory: {
		height: 150,
		width: 150,
		top: 120,
		right: '25%',
		position: 'absolute'
	}
}

function mapStateToProps({ stories }) {
	return { stories };
}
export default connect(mapStateToProps, { fetchCategory })(Dashboard);
