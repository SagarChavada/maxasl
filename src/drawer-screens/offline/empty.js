import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
import {download} from '../../helper/constants';
import { Actions } from 'react-native-router-flux';
import DeviceInfo from 'react-native-device-info';

const apiLevel = DeviceInfo.getAPILevel();
class Empty extends Component {

    constructor(props){
        super(props); 
    }

    openOfflineStories() {
        const {title, isPermission} = this.props;
        if(apiLevel >= 23) {
            if(isPermission) {
              //  console.log("isPermision", isPermission);
                if(title == "watch") {
                    Actions.watchStory();
                } else if (title == "read") {
                    Actions.readStory();
                } else {
                    Actions.wordStory();
                }
            } else {
                this.props.permission();
            }
        } else {
            if(title == "watch") {
                Actions.watchStory();
            } else if (title == "read") {
                Actions.readStory();
            } else {
                Actions.wordStory();
            }
        } 
    }
    
    render() {
    const { contentCenter, txtText, container, img } = styles;
    const {text, color} = this.props;
    return (
        <View style={container}>
            <View style={contentCenter}>
                <Text style={[txtText, {fontFamily: 'gilroy-extrabold', color: color}]}>{`Uh oh! You don't \nhave any ${text} \ndownloaded yet!`}</Text>
            </View>
            <View style={contentCenter}>
                <TouchableOpacity onPress={() => this.openOfflineStories()}>
                    <Image source={download} style={img} resizeMode={ImageResizeMode.contain}/>
                </TouchableOpacity>
            </View>
        </View>
    );
    }

}

const styles = {
    container: {
      flex: 1,  
      flexDirection: 'row'
    },
    contentCenter: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    txtText: {
      fontFamily: 'gilroy-extrabold',
      fontSize: 20
    },
    img: {
        height: 120,
        width: 220,
    }
  };

export default Empty;
