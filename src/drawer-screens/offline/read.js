import React, { Component } from 'react';
import { View, Modal, BackHandler, Platform, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux';
// import Video from 'react-native-video';
import Video from 'react-native-af-video-player';
// import VideoPlayer from 'react-native-video-controls';
import RowList from './helper/rowList';
import EmptyView from './empty';
import { listOfFile } from '../../action';
import { purple } from '../../helper/constants';

import RNFetchBlob from 'rn-fetch-blob';
let dirs = Platform.OS == 'android' ? RNFetchBlob.fs.dirs.DownloadDir : RNFetchBlob.fs.dirs.DocumentDir;

class Read extends Component {

  constructor() {
    super();
    this.showPreview = this.showPreview.bind(this);
    this.onLoad = this.onLoad.bind(this);
    this.onProgress = this.onProgress.bind(this);
    this.onBuffer = this.onBuffer.bind(this);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.showPreviewVideo = this.showPreviewVideo.bind(this);
    this.onRemoveVideo = this.onRemoveVideo.bind(this);
  }

  state = {
    files: [],
    isBuffering: false,
    duration: 0.0,
    currentTime: 0.0,
    isBuffering: false,
    paused: false,
    isModelVideo: false,
    videoUrl: '',
    title: ''
  }

  componentWillMount() {
    this.props.listOfFile();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.files != this.props.files) {
      if (!!this.props.files && this.props.files.length > 0) {
        let watcharray = [];
        let videoArray = [];
        let imageArray = []
        this.props.files.forEach((item, index) => {
          let array = item.split("_");
          if (array[0] == "read") {
            if (item.includes("png") || item.includes("jpg")) {
              imageArray.push(item)
            } else {
              videoArray.push(item);
            }
          }
        });
        videoArray.forEach((itemVideo, index) => {
          let arrayVideo = itemVideo.split("_");
          let videoid = arrayVideo[2].split(".");
          imageArray.forEach((itemImage, index) => {
            let arrayImage = itemImage.split("_");
            let name = arrayImage[1].includes("-") ? arrayImage[1].replace("-", " ") : arrayImage[0];
            let imageId = arrayImage[2].split(".");
            if (videoid[0] == imageId[0]) {
              let obj = { "video": itemVideo, "image": itemImage, "name": name, "id": videoid[0] }
              watcharray.push(obj);
            }
          })
        })
        this.setState({ files: watcharray });
      }
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    if (this.state.isModelVideo) {
      this.setState({ isModelVideo: false, paused: true, isReadStory: false });
      if (Platform.OS == "ios") {
        this.player.seek(0);
      } else {
        this.player.player.ref.seek(0);
      }
    } else {
      BackHandler.exitApp();
    }
  }

  onRemoveVideo(url, imgUrl) {
    let path = dirs + "/maxasl/" + url;
    let pathImage = dirs + "/maxasl/" + imgUrl;
    this.removeData(path)
    this.removeData(pathImage)
  }

  removeData(path) {
    RNFetchBlob.fs.unlink(path)
    .then(() => {
      this.props.listOfFile(); 
    })
    .catch((err) => {console.log(err) })
  }

  showPreview(videoUrl, title) {
    this.setState({ isModelVideo: true, paused: false, videoUrl, title });
  }

  onLoad(data) {
    this.setState({ duration: data.duration });
  }

  onProgress(data) {
    this.setState({ currentTime: data.currentTime });
    if (data.currentTime === data.duration) {
      this.onEnd();
    }
  }

  onBuffer({ isBuffering }) {
    this.setState({ isBuffering });
  }

  onEnd = () => {
    if (Platform.OS == "ios") {
      this.player.seek(0);
    } else {
      this.player.player.ref.seek(0);
    }
    this.setState({ paused: true, isModelVideo: false });
  };

  onAudioBecomingNoisy = () => {
    this.setState({ paused: true });
  };

  onAudioFocusChanged = (event) => {
    this.setState({ paused: !event.hasAudioFocus });
  };

  showPreviewVideo() {
    const { videoUrl, paused } = this.state;
    const { container } = styleInline;
    return (
      <Modal animationType="slide"
        visible={this.state.isModelVideo}
        supportedOrientations={['landscape']}
        onRequestClose={() => { this.handleBackButtonClick() }}>
        <View style={{ flex: 1 }}>
          <View>
            <View style={container}>
              {this.renderBackIcon()}
              <View style={{ flex: 1 }}>
                {this.renderTitle()}
              </View>
            </View>
          </View>
          {
            Platform.OS == "ios" ?
              <Video source={{ uri: videoUrl }}
                ref={(ref) => {
                  this.player = ref
                }}
                paused={paused}
                onLoad={this.onLoad}
                onBuffer={this.onBuffer}
                onProgress={this.onProgress}
                onEnd={this.onEnd}
                controls
                onAudioBecomingNoisy={this.onAudioBecomingNoisy}
                onAudioFocusChanged={this.onAudioFocusChanged}
                style={styles.flexOne}>
              </Video> :
              // <VideoPlayer source={{ uri: videoUrl }}
              //   ref={ref => this.player = ref}
              //   disableBack
              //   paused={paused}
              //   onLoad={this.onLoad}
              //   onBuffer={this.onBuffer}
              //   onProgress={this.onProgress}
              //   onEnd={this.onEnd}
              //   onPlay={() => { this.setState({ paused: false }) }}
              //   onPause={() => { this.setState({ paused: true }) }}
              //   controls
              //   onAudioBecomingNoisy={this.onAudioBecomingNoisy}
              //   onAudioFocusChanged={this.onAudioFocusChanged}
              //   videoStyle={styles.flexOne}>
              // </VideoPlayer>
              null
          }
        </View>
      </Modal>
    )
  }

  renderBackIcon() {
    const { iconLeft } = styleInline;
    return (
      <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.handleBackButtonClick()}>
        <Icon name='chevron-thin-left' type='Entypo' style={iconLeft} />
      </TouchableOpacity>
    )
  }

  renderTitle() {
    return <Text style={[styleInline.txtTitle]}>{this.state.title}</Text>
  }

  render() {
    const { flexOne } = styles;
    const { files } = this.state;
    return (
      <View style={flexOne}>
        {files && files.length > 0 ? <RowList array={files} color="#72b45a" showPreview={this.showPreview} onRemove={this.onRemoveVideo}/> 
        : <EmptyView title="read" color="#72b45a" text="stories" isPermission={this.props.isPermission} permission={this.props.permission} />}
        {this.showPreviewVideo()}
      </View>
    );
  }
}

const styles = {
  flexOne: {
    flex: 1
  }
};

const styleInline = {
  container: {
    flexDirection: 'row',
    height: (Platform.OS === 'ios') ? 60 : 50,
    backgroundColor: purple,
    paddingTop: (Platform.OS === 'ios') ? 10 : 0
  },
  iconLeft: {
    color: "#FFF",
    marginTop: 10,
    marginLeft: 10,
  },
  txtTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 15,
    color: '#FFF',
    alignSelf: 'center',
  }
}

function mapStateToProps({ stories, files }) {
  return { stories, files };
}
export default connect(mapStateToProps, { listOfFile })(Read);
