import React, { Component } from 'react'
import { View, TouchableOpacity, Text, PermissionsAndroid, Platform } from 'react-native';
import styles from '../../styles';
import Appbar from '../../helper/appbar';
import Bottomline from '../../helper/bottomline';
import RNFetchBlob from 'rn-fetch-blob';
import Watch from './watch';
import Read from './read';
import Vocabulary from './vocabulary';

const permissions = [
    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
]

let dirs = Platform.OS == 'android' ? RNFetchBlob.fs.dirs.DownloadDir : RNFetchBlob.fs.dirs.DocumentDir;
class Offline extends Component {

    constructor(props) {
        super(props)

        this.changeContentStyle = this.changeContentStyle.bind(this);
        this.renderView = this.renderView.bind(this);
        this.storagePermission = this.storagePermission.bind(this);
    }

    checkExist() {
        RNFetchBlob.fs.exists(dirs + `/maxasl/`)
          .then((exist) => {
              if(!exist) {
                this.makeDir(); 
              }
          })
          .catch(() => { console.log("Error exist") })
      }
    
      makeDir() {
        RNFetchBlob.fs.mkdir(dirs + `/maxasl/`)
        .then(() => { this.checkExist() })
        .catch((err) => { console.log("Error mkdire >>", err) })
      }

    componentDidMount() {
        this.storagePermission();
    }

    state = {
        watch: true,
        read: false,
        vocabulary: false,
        isPermission: false
    }

    changeContentStyle(title) {
        if (title == "Watch") {
            this.setState({ watch: true, read: false, vocabulary: false });
        } else if (title == "Read") {
            this.setState({ watch: false, read: true, vocabulary: false });
        } else {
            this.setState({ watch: false, read: false, vocabulary: true });
        }
    }

    renderView() {
        const { watch, read, isPermission } = this.state;
        if (watch) {
            return <Watch isPermission={isPermission} permission={this.storagePermission}/>;
        } else if (read) {
            return <Read isPermission={isPermission} permission={this.storagePermission}/>;
        } else {
            return <Vocabulary isPermission={isPermission} permission={this.storagePermission}/>;
        }
    }

    renderButtons(title, isVisible, color) {
        return (
            <TouchableOpacity style={{ backgroundColor: isVisible ? color : '#FFF', padding: 18 }} onPress={() => this.changeContentStyle(title)}>
                <Text style={{ color: isVisible ? '#FFF' : '#274A9E', fontFamily: 'gilroy-extrabold', fontSize: 16 }}>{title}</Text>
            </TouchableOpacity>
        )
    }

    storagePermission() {
        try {
            PermissionsAndroid.requestMultiple(permissions).then(granted => {
                this.setState({ isPermission: true });
                this.checkExist();
            });
        } catch (err) {
            console.warn(err)
        }        
    }

    render() {
        const { container } = styles;
        const { watch, read, vocabulary } = this.state;
        return (
            <View style={container}>
                <Appbar icon={false} title="Watch Offline" color="#72b45a" />
                <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#EEE' }}>
                    <View style={{ width: 100 }} />
                    {this.renderButtons("Watch", watch, "#274A9E")}
                    {this.renderButtons("Read", read, "#72b45a")}
                    {this.renderButtons("Vocabulary", vocabulary, "#EC813D")}
                </View>
                <View style={{ flex: 1 }}>
                    {this.renderView()}
                </View>

                <Bottomline />
            </View>
        )
    };
}

export default Offline;
