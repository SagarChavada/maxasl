import React, { Component } from 'react';
import { View, Image, ImageBackground, FlatList, TouchableOpacity, Platform, ToastAndroid } from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
import {Icon} from 'native-base';
import RNFetchBlob from 'rn-fetch-blob';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import OfflineCard from './helper/offlinecard';
import {starline, downloadselected, selectedall} from '../../helper/constants';
import {RenderTitle} from './helper';
import { listOfFile } from '../../action';
import Toast from 'react-native-toast-native';

let dirs = Platform.OS == 'android' ? RNFetchBlob.fs.dirs.DownloadDir : RNFetchBlob.fs.dirs.DocumentDir;

const style={
  backgroundColor: ( Platform.OS === 'ios' ) ? '#000000' : "#EEEEEE",
  width: 300,
  height: 50,
  fontSize: 18,
  borderRadius: 15,
};

class ReadStory extends Component {

  constructor(props){
    super(props);
    this.onSelectCard = this.onSelectCard.bind(this);
    this.onSelectAll = this.onSelectAll.bind(this);
    this.onSelectedDownload = this.onSelectedDownload.bind(this);
  }

  state = {
    stories: [],
    selectedStory: [],
    files: []
  }

  componentWillMount() {
    const storyData = [];
    const data = this.props.fetchAll.story_sentance_videos;
    data.map((item) => {
      item.map((item) => {
        storyData.push(item);
      });
    })
    this.setState({ stories: storyData, files: this.props.files });
  }

  componentDidMount() {
    const {files, stories} = this.state
    if(!!files && files.length > 0) {
      files.forEach((item, index) => {
        if(!item.includes(".DS_Store")) {
        let array = item.split("_");
        let id = array[2].split(".");
        let type = array[0];
        stories.forEach((item, index) => {
          if(item.id == id[0] && type == "read") {
            setTimeout(() => {
                this.setState({stories: update(this.state.stories, 
                  {[index]: {is_selected: {$set: false}, start_download: {$set: false}, is_downloaded: {$set: true}}})});
            }, 100)
          }
        });
      }
      })
    }
  }

  componentDidUpdate(prevProps) {
    if(prevProps.files != this.props.files) {
        this.setState({ files: this.props.files }, () => {
          if(!!this.state.files && this.state.files.length > 0) {
            this.state.files.forEach((item) => {
              if(!item.includes(".DS_Store")) {
              let array = item.split("_");
              let id = array[2].split(".");
              let type = array[0];
              this.state.stories.forEach((item, index) => {
                if(item.id == id[0] && type == "read") {
                  setTimeout(() => {
                      this.setState({stories: update(this.state.stories, 
                        {[index]: {is_selected: {$set: false}, start_download: {$set: false}, is_downloaded: {$set: true}}})});
                  }, 100)
                }
              });
            }
            })
          }
        });
    }
  }

  renderSelectAll() {
    return(
      <TouchableOpacity onPress={() => this.onSelectAll()}>
        <Image source={selectedall} />
      </TouchableOpacity>
    );
  }

  renderDowloadSelected() {
    return(
      <TouchableOpacity onPress={() => this.onSelectedDownload()}>
        <Image source={downloadselected} />
      </TouchableOpacity>
    );
  }

  showToast(title) {
    if(Platform.OS == "ios") {
        Toast.show(title,Toast.SHORT, Toast.TOP, style);
    } else {
        ToastAndroid.showWithGravity(
            title,
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
          );
    }
  }

  storeDatatoStorage(type, id, name, ext, url) {
    RNFetchBlob
      .config({
        addAndroidDownloads : {
          useDownloadManager: true,
          notification: true,
          description : `${name} download`,
          mediaScannable: true,
          mime : 'video/mp4',
          path:  `file://${dirs}/maxasl/${type}_${name}_${id}.${ext}`
        },
        IOSBackgroundTask: true,
        fileCache: true,
        appendExt: ext,
        path: dirs + `/maxasl/${type}_${name}_${id}.${ext}`
      })
      .fetch('GET', url, {
        //some headers ..
      })
      .then((res) => {
        this.props.listOfFile();
      })
  }

  storeImagetoStorage(type, id, name, ext, url) {
    RNFetchBlob
      .config({
        addAndroidDownloads : {
          useDownloadManager: true,
          notification: true,
          description : `${name} download`,
          mediaScannable: true,
          mime : 'video/mp4',
          path:  `file://${dirs}/maxasl/${type}_${name}_${id}.${ext}`
        },
        fileCache: true,
        appendExt: ext,
        path: dirs + `/maxasl/${type}_${name}_${id}.${ext}`
      })
      .fetch('GET', url, {
        //some headers ..
      })
      .then((res) => {
        this.setState({ filePath: res.path() });
      })
  }

  GetFilename(url)
    {
      if (url)
      {
          var m = url.toString().match(/.*\/(.+?)\./);
          if (m && m.length > 1)
          {
            return m[1];
          }
      }
      return "";
    }

  onSelectedDownload() {
    const { stories } = this.state;
    let selected = [];
    stories.forEach((item, index) => {
      if (item.is_selected) {
        selected.push(item);
        setTimeout(() => {
            this.setState({stories: update(this.state.stories, {[index]: {start_download: {$set: true}, is_selected: {$set: false}}})});
        }, 100)
      }
    });
    this.setState({ selectedStory: selected }, () => {
      if(!!this.state.selectedStory && this.state.selectedStory.length > 0) {
        this.showToast("download started...")
        selected = [];
        this.state.selectedStory.map((item) => {
            let url = item.video_url;
            let image = item.tumb_images;
            let videoTitle = item.video_title.replace(/ /g,"-");
            let videoExt = url.split('.').pop();
            let imageExt = image.split('.').pop();
            this.storeDatatoStorage('read', item.id, videoTitle, videoExt, url);
            this.storeImagetoStorage('read', item.id, videoTitle, imageExt, image);
        })
      } 
    });
    stories.forEach((item, index) => {
      if (item.is_selected) {
        setTimeout(() => {
            this.setState({stories: update(this.state.stories, {[index]: {start_download: {$set: true}, is_selected: {$set: false}}})});
        }, 100)
      }
    });
  }

  onSelectAll() {
    const { stories } = this.state;
    stories.forEach((item, index) => {
      setTimeout(() => {
        if(!item.is_downloaded){
          this.setState({stories: update(this.state.stories, {[index]: {is_selected: {$set: true}}})});
        }
      }, 100)
    });
  }

  onSelectCard(id, isSelect) {
    const { stories } = this.state;
    stories.forEach((item, index) => {
      if(id == index) {
        this.setState({stories: update(this.state.stories, {[index]: {is_selected: {$set: !isSelect}}})});
      }
    });
  }

  renderGrid() {
    const { stories } = this.state;
    if(!!stories && stories.length > 0) {
      return(
        <FlatList
            contentContainerStyle={{alignItems: 'center'}}
            data={this.state.stories}
            style={styles.listContainer}
            renderItem={({item, index}) => (<OfflineCard data={item} index={index} type="read" onSelect={this.onSelectCard} />)}
            numColumns={3}
        />
      );
    }
  }

  render() {
    const { container, iconClose } = styles;
    return (
      <View style={{flex: 1}}>
          <ImageBackground source={starline} style={container} resizeMode={ImageResizeMode.contain}>
            <Icon name="close" style={iconClose} type="FontAwesome" onPress={() => {Actions.pop()}}/>
            <RenderTitle text="Stories to Read Offline" />
            <View style={{flexDirection: 'row', alignItems: 'space-around', marginTop: 10}}>
              {this.renderSelectAll()}
              <View style={{ width: 50}} />
              {this.renderDowloadSelected()}
            </View>
            {this.renderGrid()}
          </ImageBackground>
      </View>
    );
  }

}

const styles = {
  container: {
    flex: 1,  
    backgroundColor: '#72B45A',
    alignItems: 'center',
    width: '100%', height: 120,
  },
  iconClose: {
    color: '#FFF',
    fontSize: 30,
    alignSelf: 'flex-end',
    marginHorizontal: 20,
    marginTop: 20,
    fontWeight: 'bold',
  },
  listContainer: {
    marginVertical: 10,
    marginHorizontal: 10,
  },
};

function mapStateToProps({ stories, files, fetchAll }) {
	return { stories, files, fetchAll };
}
export default connect(mapStateToProps, {listOfFile})(ReadStory);
