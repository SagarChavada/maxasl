import React, { Component } from 'react';
import {View, Text, ImageBackground, TouchableOpacity, ActivityIndicator } from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
import { Icon } from 'native-base';
import styles from '../../../styles';


class OfflineCard extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        
    }

    renderLockImage(color) {
        return (
            <View style={stylesInline.circleLockView}>
                <Icon name="check" type="SimpleLineIcons" style={[stylesInline.lockImage, {color: color}]} />
            </View>
        )
    }

    renderSpinner() {
        return (
            <View style={stylesInline.circleLockView}>
                <ActivityIndicator size="large" color="#F7CD52" />
            </View>
        )
    }

    render() {
        const { cardContainer, cardImage } = styles;
        const { title, is_selected, start_download, is_downloaded, tumb_images, video_title } = this.props.data;
        const {type} = this.props;
        return (
            <TouchableOpacity key={this.props.index} disabled={is_downloaded} style={cardContainer} onPress={() => this.props.onSelect(this.props.index, is_selected)}>
                <Text style={[{ fontSize: 17, fontFamily: 'gilroy-extrabold', alignSelf: 'center', marginTop: 5, color: '#1D358C' }]}>
                    {type !== 'watch' ? video_title : title}
                </Text>
                <ImageBackground
                    resizeMode={ImageResizeMode.contain}
                    source={{ uri: tumb_images }}
                    style={[cardImage, { height: 140, width: 140 }]}>
                    {start_download && this.renderSpinner()}
                    {is_selected && this.renderLockImage("#FFF")}
                    {is_downloaded && this.renderLockImage("#2ecc71")}
                </ImageBackground>
            </TouchableOpacity>
        );
    }
}

const stylesInline = {
    lockImage: {
        fontSize: 50
    },
    circleLockView: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: '#00000090'
    },
    borders: {
        borderRadius: 50,
        borderWidth: 3,
        borderColor: '#FFF',
        padding: 8,
    },
}

export default OfflineCard;
