import Swipeout from 'react-native-swipeout';

import React, {Component} from 'react';
import {Text, View, TouchableWithoutFeedback, TouchableOpacity, Image, Platform} from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
import { Icon } from 'native-base';
import RNFetchBlob from 'rn-fetch-blob';
import { purple, watchnow } from '../../../helper/constants';

let dirs = Platform.OS == 'android' ? RNFetchBlob.fs.dirs.DownloadDir : RNFetchBlob.fs.dirs.DocumentDir;
//  example swipout app
class RowList extends Component {

  constructor() {
    super();
    this.state = {
      sectionID: null,
      rowID: null,
    };
  }

  _renderRow(item, sectionID, rowID) {
    const filePathImage = Platform.OS == 'android' ? `file://${dirs}/maxasl/${item.image}` : `${dirs}/maxasl/${item.image}`
    const swipeoutBtns = [
      {
        component: [
          <TouchableOpacity 
              key={rowID}
              onPress={() => this.props.onRemove(item.video, item.image)}
              style={{
                width: 160,
                height: 75,
                backgroundColor: purple,
                flexDirection: 'row',
                justifyContent: 'center'
              }}>
              <Icon style={{ alignSelf: 'center', color: '#FFF', marginRight: 10, fontSize: 18}} name='close' type='FontAwesome' />
              <Text style={{ color: '#FFF', alignSelf: 'center', fontSize: 16, fontFamily: 'Gilroy-extrabold'}}>Remove</Text>
            </TouchableOpacity>
        ]
      }
    ]
    return (

      <Swipeout
        key={rowID}
        close={!(this.state.sectionID === sectionID && this.state.rowID === rowID)}
        right={swipeoutBtns}
        rowID={rowID}
        sectionID={sectionID}
        autoClose={true}
        backgroundColor={'#FFF'}
        buttonWidth={160}
        onOpen={(sectionID, rowID) => {
          this.setState({
            sectionID,
            rowID,
          })
        }}
      >
        <TouchableWithoutFeedback>
          <View style={styles.rowFront}>
            <Image source={{ uri: filePathImage }} 
            resizeMode={ImageResizeMode.cover} style={[{ height: 60, width: 140 }]}/>
            <Text style={[styles.txtTitle, {color: this.props.color}]}>{item.name}</Text>
            <View style={{flex: 1}}>
                <TouchableOpacity style={{ alignSelf: "flex-end"}} 
                  onPress={() => this.props.showPreview(dirs + "/maxasl/" + item.video, item.name)}>
                  <Image source={watchnow} resizeMode={ImageResizeMode.contain}/>
                </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Swipeout>
    );
  }

  render() {
    return this.props.array.map((item, index) => {
      return this._renderRow(item, index, index)
    });
  }
}

const styles = {
  rowFront: {
    height: 75,
    borderBottomColor: '#EEE',
    borderBottomWidth: 1,
    alignItems: 'center',
    paddingRight: 100,
    paddingLeft: 100,
    flexDirection: 'row',
  },
  txtTitle: {
    fontFamily: 'Gilroy-extrabold',
    fontSize: 18,
    marginLeft: 20,
  }
}

export default RowList;