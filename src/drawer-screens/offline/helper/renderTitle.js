import React from 'react';
import createReactClass from 'create-react-class';
import { Text } from 'react-native';

const RenderTitle = createReactClass({
    render() {
        return <Text style={{ fontSize: 22, fontFamily: 'gilroy-extrabold', color: "#FFF", }}>{this.props.text}</Text>;
    }
});

export default RenderTitle;
