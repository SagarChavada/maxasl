import React, { Component } from 'react'
import { View, ScrollView, Image, Text, Dimensions, TouchableOpacity, Linking, Modal, BackHandler, Platform } from 'react-native';
import { Icon } from 'native-base';
import Video from 'react-native-af-video-player';
// import Video from 'react-native-video';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
// import VideoPlayer from 'react-native-video-controls';
import styles from '../styles';
import Appbar from '../helper/appbar';
import { about_btn, MaxASL_App, boy, girlbook, plane, thumb, heart, earth, purple, PLAY_STORE, APP_STORE } from '../helper/constants';
import BottomLine from '../helper/bottomline';

class Setting extends Component {

    constructor() {
        super();
        this.showPreview = this.showPreview.bind(this);
        this.onLoad = this.onLoad.bind(this);
        this.onProgress = this.onProgress.bind(this);
        this.onBuffer = this.onBuffer.bind(this);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.showPreviewVideo = this.showPreviewVideo.bind(this);
    }

    state = { 
        orientation: '', 
        aboutUrl: "http://13.232.167.42/max-asl/public/upload/aboutus.mp4",
        isBuffering: false,
        duration: 0.0,
        currentTime: 0.0,
        isBuffering: false,
        paused: false,
        isModelVideo: false,
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentDidMount()
    {
        this.getOrientation();
        
        Dimensions.addEventListener( 'change', () => {
            this.getOrientation();
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
  
    handleBackButtonClick() {
        if (this.state.isModelVideo) {
          this.setState({ isModelVideo: false, paused: true, isReadStory: false });
          if(Platform.OS == "ios") {
            this.player.seek(0);
          } else {
            this.player.player.ref.seek(0);
          }
        } else {
          BackHandler.exitApp();
        }
    }

    getOrientation = () =>
    {
        if( this.refs.rootView )
        {
            if( Dimensions.get('window').width < Dimensions.get('window').height )
            {
            this.setState({ orientation: 'portrait' });
            }
            else
            {
            this.setState({ orientation: 'landscape' });
            }
        }
    }

    showPreview() {
        this.setState({ isModelVideo: true, paused: false });
    }


    onLoad(data) {
        this.setState({ duration: data.duration });
    }

    onProgress(data) {
        this.setState({ currentTime: data.currentTime });
        if (data.currentTime === data.duration) {
        this.onEnd();
        }
    }

    onBuffer({ isBuffering }) {
        this.setState({ isBuffering });
    }

    onEnd = () => {
        if(Platform.OS == "ios") {
            this.player.seek(0);
          } else {
            this.player.player.ref.seek(0);
          }
        this.setState({ paused: true, isModelVideo: false });
    };

    onAudioBecomingNoisy = () => {
        this.setState({ paused: true });
    };

    onAudioFocusChanged = (event) => {
        this.setState({ paused: !event.hasAudioFocus });
    };

    showPreviewVideo() {
        const { aboutUrl, paused } = this.state;
        const { container } = styleInline;
        return(
            <Modal animationType="slide"
                    visible={this.state.isModelVideo}
                    supportedOrientations={['landscape']}
                    onRequestClose={() => { this.handleBackButtonClick() }}>
                <View style={{flex: 1}}>
                    <View>
                        <View style={container}>
                            {this.renderBackIcon()}
                            <View style={{flex: 1}}>
                                {this.renderTitle()}
                            </View>  
                        </View>
                    </View>
                    {
                        Platform.OS == "ios" ?
                            <Video source={{ uri: aboutUrl }}
                                ref={(ref) => {
                                    this.player = ref
                                }}
                                paused={paused}
                                onLoad={this.onLoad}
                                onBuffer={this.onBuffer}
                                onProgress={this.onProgress}
                                onEnd={this.onEnd}
                                controls
                                onAudioBecomingNoisy={this.onAudioBecomingNoisy}
                                onAudioFocusChanged={this.onAudioFocusChanged}
                                style={styles.backgroundVideo}>
                            </Video> :
                            // <VideoPlayer source={{ uri: aboutUrl }}
                            //     ref={ref => this.player = ref}
                            //     disableBack
                            //     paused={paused}
                            //     onLoad={this.onLoad}
                            //     onBuffer={this.onBuffer}
                            //     onProgress={this.onProgress}
                            //     onEnd={this.onEnd}
                            //     onPlay={() => { this.setState({ paused: false }) }}
                            //     onPause={() => { this.setState({ paused: true }) }}
                            //     controls
                            //     onAudioBecomingNoisy={this.onAudioBecomingNoisy}
                            //     onAudioFocusChanged={this.onAudioFocusChanged}
                            //     videoStyle={styles.backgroundVideo}>
                            // </VideoPlayer>
                            null
                    }
                </View>
            </Modal>
        )
    }

    renderBackIcon() {
        const { iconLeft } = styleInline;
        return(
            <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.handleBackButtonClick()}>
                <Icon name='chevron-thin-left' type='Entypo' style={iconLeft} />
            </TouchableOpacity>
        )
    }
    
    renderTitle() {
        return <Text style={[styleInline.txtTitle]}>About MaxASL</Text>
    }

  render() {
    const { container, contentContainer, containerPortrait, 
        containerlistborder, jfyAlign, flexDirectionRow, imgstyle, margVertical10, flexOne} = styles;
    return (
        <View style={( this.state.orientation == 'portrait' ) ? containerPortrait : container} ref = "rootView" >
            <Appbar icon={false} title="Settings" color="#56B1E9" />
            <ScrollView>
            <View style={contentContainer}>
                <View style={[flexOne, flexDirectionRow]}>
                    <View style={{justifyContent: 'center', alignItems: 'center', 
                    marginVertical: 20, flex: ( this.state.orientation == 'portrait' ) ? 0.6 : 1}}>
                        <Image 
                            source={MaxASL_App} 
                            style={{ height: 100, width: 100}}
                            resizeMode={ImageResizeMode.contain}
                            />
                    </View>
                    <TouchableOpacity style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}} onPress={() => this.showPreview() }>
                        <Image source={about_btn} 
                            style={{ height: 120, width: 220, marginRight: 30}}
                            resizeMode={ImageResizeMode.contain}
                        />
                    </TouchableOpacity>
                </View>
                <View style={[containerlistborder,margVertical10]}>
                    {this.renderlist('#274A9E', 'Contact Us', 'angle-right', 'FontAwesome', plane, 'contactus')}
                    {this.renderlist('#274A9E', 'Privacy Policy', 'angle-right', 'FontAwesome', heart, 'privacy')}
                    {this.renderlist('#274A9E', 'Terms of Use', 'angle-right', 'FontAwesome', thumb, 'term')}
                    {this.renderlist('#274A9E', 'Visit our Website', 'angle-right', 'FontAwesome', earth, 'website')}
                </View>
                <View style={[jfyAlign, margVertical10]}>
                    {this.renderLabel('#B0071F', 'Love this? Write a review!', 'heart', 'Entypo', 'review')}
                </View>
                <View style={[jfyAlign, flexDirectionRow, margVertical10]}>
                    {this.renderBox('#fff', 'Share on Facebook', 'facebook', 'FontAwesome', '#274A9E', 'facebook')}
                    {this.renderBox('#fff', 'Share on Twitter', 'twitter', 'FontAwesome', '#72B45A', 'twitter')}
                    {this.renderBox('#fff', 'Share on Instagram', 'instagram', 'FontAwesome', '#EC813D', 'instagram')}
                </View>
                <View style={[jfyAlign,margVertical10, {flexDirection: 'row'}]}>
                    <Image source={girlbook} style={imgstyle} resizeMode={ImageResizeMode.contain}/>
                    <Image source={boy} style={imgstyle} resizeMode={ImageResizeMode.contain}/>
                </View>
            </View>
            </ScrollView>
            <BottomLine />

            {this.showPreviewVideo()}
        </View>
    )
    };

    handleLinks(type) {
        switch (type) {
            case "contactus":
                Linking.openURL('mailto:apps@kodalearning.com')
                break;
            case "privacy":
                Linking.openURL('http://maxasl.com/privacy-policy/')
                break;
            case "term":
                Linking.openURL('http://maxasl.com/terms-of-use/')
            break;  
            case "website":
            Linking.openURL('http://maxasl.com/')
                break;
            case "facebook":
            Linking.openURL('https://www.facebook.com/MaxASLApp')
                break; 
            case "instagram":
            Linking.openURL('https://www.instagram.com/MaxASLApp')
                break; 
            case "twitter":
            Linking.openURL('https://twitter.com/MaxASLApp')
                break;
            case "review":
                if(Platform.OS =='ios'){
                    Linking.openURL(APP_STORE).catch(err => console.error('An error occurred', err));
                }
                else{
                    Linking.openURL(PLAY_STORE).catch(err => console.error('An error occurred', err));
                }
                break;              
            default:
                break;
        }
    }

    renderlist(color, title, icon, type, iconname, linktype) {
        const { flexDirectionRow, jfyAlign, textSize20 ,containermainlist, flexOne, imgicon} = styles;
    
        return (
          <TouchableOpacity style={[flexDirectionRow, containermainlist]} onPress={() => this.handleLinks(linktype)}>
            <View style={{flex: 0.3}} />
            <View style={[flexDirectionRow, flexOne]}>
              <Image source={iconname} style={imgicon} resizeMode={ImageResizeMode.contain}/>
              <Text style={[textSize20, { color: color, left: 30 }]}>{title}</Text>
            </View>
            <View style={{flex: 0.2}}>
              <Icon name={icon} type={type} style={{ color: color }} />
            </View>
          </TouchableOpacity>
        )
      }

      renderLabel(color, title, icon, type, linktype) {
        const { innerlabel, flexDirectionRow,textSize20 } = styles;
        return (
          <TouchableOpacity style={[innerlabel,flexDirectionRow]} onPress={() => this.handleLinks(linktype)}>
            <Icon name={icon} type={type} style={{ color: color, marginTop: 5 }} />
            <Text style={[textSize20, { color: color, marginLeft: 5 }]}>{title}</Text>
          </TouchableOpacity>
        )
      }

      renderBox(color, title, icon, type, backgroundColor, linktype) {
        const { innerContainerBox,jfyAlign, textSize17 } = styles;
    
        return (
          <TouchableOpacity style={[innerContainerBox,jfyAlign, { backgroundColor: backgroundColor }]} onPress={() => this.handleLinks(linktype)}>
            <Icon name={icon} type={type} style={{ color: color }} />
            <Text style={[textSize17, { color: color, marginTop: 5 }]}>{title}</Text>
          </TouchableOpacity>
        )
      }
}

const styleInline = {
    container: {
        flexDirection: 'row',
        height: ( Platform.OS === 'ios' ) ? 60 : 50,
        backgroundColor: purple,
        paddingTop: ( Platform.OS === 'ios' ) ? 10 : 0
    },
    iconLeft: {
        color: "#FFF",
        marginTop: 10,
        marginLeft: 10,
    },
    txtTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 15,
        color: '#FFF',
        alignSelf: 'center',
    }
}

export default Setting;
