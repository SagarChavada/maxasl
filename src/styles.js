import { StyleSheet, Platform, Dimensions } from 'react-native';
import {purple} from './helper/constants';
const { height, width } = Dimensions.get('window');
export default styles = StyleSheet.create({
    flexOne: {
        flex: 1,
    },
    backgroundVideo: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top:0,
        left: 0,
        bottom: 0,
        right: 0,
      },
    containerMain: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 10
    },
    imgicon: {
        width: 30,
        height: 30
    },  
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    cardContainer: {
        backgroundColor:'#fff',
        borderRadius: 8,
        padding: 8,
        margin: 6,
        maxWidth: 200
    },
    bottomContainer: {
        marginTop: 60,
        backgroundColor: '#FFF',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        height: 70,
        marginHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    containerPortrait: {
        flex: 1,
        backgroundColor: "#FFF",
        paddingTop: ( Platform.OS === 'ios' ) ? 35 : 0
    },
    imgBackground: {
        flex:1,
        width: '100%',
        height: 120,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    listContainer: {
        marginVertical: 10,
        marginHorizontal: 10,
        backgroundColor: purple
    },
    backgroundImage: {
        width: '100%',
        height: 100,
        backgroundColor: purple
    },
    storyBanner: {
        width: '100%',
        height: 250,
    },  
    contentContainer: {
        flex:1
    },
    imgButtonLabel: {
        alignSelf: 'flex-end', flex: 1, margin: 8,
    },
    imgLabel: {
        height: 60, width: 220,
        alignSelf: 'center'
    },  
    cardMainView:{
        backgroundColor:'#fff',
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        marginRight: 8
    },
    cardTextTitle:{
        color:'#1D358C',
        alignSelf:'center',
        marginTop:15,
    },
    cardImageView:{
        flex: 1,
    },  
    cardImage:{
        marginTop: 15,
        alignSelf: 'center',
    },
    videoPlayIcon:{
        color:'#1D358C',
        fontSize:40,
    },
    heartIcon: {
        color: purple,
        fontSize:30,
    },
    innerlabel: {
        borderColor: 'red',
        borderRadius: 10,
        borderWidth: 1,
        paddingHorizontal: 50,
        paddingVertical: 3,
        alignItems: 'center',
      },
      innerContainerBox: {
        height: 100,
        borderRadius: 10,
        marginHorizontal: 5,
        width: '30%'
      },
      jfyAlign: {
        alignItems: 'center',
        justifyContent: 'center'
      },
      flexDirectionRow: {
        flexDirection: 'row',
      },
      imgstyle: {
        height: 200,
      },
      margVertical10: {
        marginVertical: 10
      },
      containerlistborder: {
        borderBottomColor: '#EEE',
        borderBottomWidth: 0.5,
      },
      About_Btn: {
        height: 80,
        width: 280,
      },
      About_BtnP: {
        height: 60,
        width: 170,
      },
      AppIcon: {
        height: 150,
        width: 150,
      },
      AppIconPortrait: {
        height: 100,
        width: 100,
      },
      marginVertical25: {
         flex: 1,
         marginVertical: 25,
         },
      textSize20: {
        fontSize: 20,
        textAlign: 'center',
      },
      textSize17: {
        fontSize: 17,
        textAlign: 'center',
      },
      containermainlist: {
        paddingVertical: 10,
        borderTopWidth: 0.5,
        borderTopColor: '#EEE',
        alignItems: 'center'
    },
    footerLeftErroView:{
        left: 20,
        position: "absolute",
        flex: 1,
        top: 190,
    },
    footerRightErroView:{
        flex: 1,
        right: 20,
        top: 190,
        position: "absolute",
    },
    footerErroImage:{
        height:50, 
        width:50,
    },
});