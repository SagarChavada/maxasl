// Normal Request
import { SERVER_URL, VIDEO, PURCHASE, GET_ALL } from '../helper/constants';
import { Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import RNFetchBlob from 'rn-fetch-blob';
import * as RNIap from 'react-native-iap';

const itemSkus = Platform.select({
	ios: [
		'max_asl_unlimited_access'
	],
	android: [
		'max_asl_unlimited_access'
	]
});

export const FETCH_ITEM = 'fetch_item';
export const getItems = () => async (dispatch, getState, api) => {
	try {
		const products = await RNIap.getProducts(itemSkus);
	//	console.log('Products', products);
		dispatch({
			type: FETCH_ITEM,
			payload: products
		});
	} catch (err) {
		console.warn(err.code, err.message);
	}
}

const deviceId = DeviceInfo.getUniqueID();
//console.log("DeviceID >> ",deviceId);
export const PURCHASE_USER_API = 'fetch_purchase';
export const getPurchase = (recipt) => async (dispatch, getState, api) => {
	const dataUser = `device_id=${deviceId}`;
	//console.log(SERVER_URL + PURCHASE, dataUser)
	const res = await api.post(SERVER_URL + PURCHASE, dataUser);
	//console.log(res);
	dispatch({
		type: PURCHASE_USER_API,
		payload: res
	});
}

let dirs = Platform.OS == 'android' ? RNFetchBlob.fs.dirs.DownloadDir : RNFetchBlob.fs.dirs.DocumentDir;
// Proxy Request for fetching category
export const FETCH_CATEGORY = 'fetch_all_category';
const data = `device_id=${DeviceInfo.getUniqueID()}`;
export const fetchCategory = () => async (dispatch, getState, api) => {
	const res = await api.post(SERVER_URL + VIDEO, data);
	//console.log("DeviceID >> ",deviceId);
	dispatch({
		type: FETCH_CATEGORY,
		payload: res
	});
};

export const FETCH_ALL = 'fetch_all';
export const getAll = () => async (dispatch, getState, api) => {
	const data = `device_id=${DeviceInfo.getUniqueID()}`;
	const res = await api.post(SERVER_URL + GET_ALL, data);
	dispatch({
		type: FETCH_ALL,
		payload: res
	});
}

export const FETCH_FILES = 'fetch_all_files';
export const listOfFile = () => (dispatch) => {
	RNFetchBlob.fs.ls(dirs + `/maxasl/`)
		// files will an array contains filenames
		.then((files) => {
			dispatch({
				type: FETCH_FILES,
				payload: files
			});
		})
}
