import React, { Component } from 'react'
import { DrawerNavigator } from 'react-navigation';
import { Root } from 'native-base';
import { connect } from 'react-redux';
import Sidemenu from '../../helper/sidemenu';
import Offline from '../../drawer-screens/offline';
import Setting from '../../drawer-screens/setting';

import { fetchCategory, getItems, getAll } from '../../action';
import Dashboard from '../../drawer-screens/dashboard';

class NavigationView extends Component {

    componentWillMount() {
        this.props.fetchCategory();
        this.props.getAll();
    }

    render() {
        return (
            <Root>
                <MainView />
            </Root>
        )
    };
}

const MainView = DrawerNavigator({
    Dashboard: {
        screen: Dashboard,
    },
    Offline: {
      screen: Offline,
    },
    Setting: {
      screen: Setting,
    },
  }, {
      initialRouteName: 'Dashboard',
      contentComponent: Sidemenu,
      drawerOpenRoute: 'DrawerOpen',
      drawerCloseRoute: 'DrawerClose',
      drawerToogleRoute: 'DrawerToogle'
  });

export default connect(null, { fetchCategory, getItems, getAll })(NavigationView);
