import React, { Component } from 'react';
import { View, Dimensions, ImageBackground, Image, Text, FlatList, 
	ScrollView, TouchableOpacity, BackHandler, Platform, Modal } from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'
import { Icon } from 'native-base';
import Video from 'react-native-video';
import Slider from 'react-native-slider';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';

import Appbar from '../../helper/appbar';
import BottomLine from '../../helper/bottomline';
import SubStoryCard from '../../helper/storycard/subStorycard';
import {
	labelRead, labelWatch, words, starbackground, purple, btnbackward, btnforward,
	subtitle, subtitlefill, watchmore, bottomfooter, btnreplay, btnreadstory, btnvocabulary, btnreadagain
} from '../../helper/constants';
import styles from '../../styles';
import { Actions } from 'react-native-router-flux';
const { height, width } = Dimensions.get('window');

class Story extends Component {

	constructor(props) {
		super(props);
		this.showPreview = this.showPreview.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.onProgress = this.onProgress.bind(this);
		this.onBuffer = this.onBuffer.bind(this);
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
		this.showPreviewVideo = this.showPreviewVideo.bind(this);
		this.openMainVideo = this.openMainVideo.bind(this);
		this.openReadStory = this.openReadStory.bind(this);
		this.openVocaStory = this.openVocaStory.bind(this);
		this.showAgain = this.showAgain.bind(this);
		this.showNext = this.showNext.bind(this);
		this.openFromCard = this.openFromCard.bind(this);

		wordVideo = this.props.wordVideo;
	}

	state = {
		readVideo: this.props.readVideo,
		orientation: '',
		column: 3,
		previewUrl: '',
		normalUrl: '',
		storyUrl: '',
		vocabularyUrl: '',
		storyTitle: '',
		title: '',
		isBuffering: false,
		duration: 0.0,
		currentTime: 0.0,
		isBuffering: false,
		paused: false,
		isModelVideo: false,
		isSub: false,
		isMainVideo: false,
		isReadVideo: false,
		isVocVideo: false,
		read: false,
		voc: false,
		currentVideo: 'normal',
		counter: 0,
		showNext: false,
		subtitle: '',
		volume: 1,
		videoMaxTime: 0,
		progressVideoValue: 0,
		progressSliderValue: 0,
		fullScreen: false,
		hideControls: false,
	}

	componentWillMount() {
		BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
	}

	handleBackButtonClick() {
		if (this.state.isModelVideo) {
			this.setState({ isModelVideo: false, paused: true, isReadStory: false });
			if (Platform.OS == "ios") {
				this.player.seek(0);
			} else {
				// this.player.player.ref.seek(0);
			}
		} else {
			BackHandler.exitApp();
		}
	}

	componentDidMount() {
		this.getOrientation();

		Dimensions.addEventListener('change', () => {
			this.getOrientation();
		});
	}

	getOrientation = () => {
		if (this.refs.rootView) {
			if (width < height) {
				this.setState({ orientation: 'portrait' });
			}
			else {
				this.setState({ orientation: 'landscape' });
			}
		}
	}


	renderBannerView() {
		const { storyBanner, containerMain, imgButtonLabel, imgLabel } = styles;
		const { banner } = this.props
		return (
			<ImageBackground source={{ uri: banner }} style={storyBanner} resizeMode={ImageResizeMode.stretch}>
				<View style={containerMain}>
					<TouchableOpacity style={imgButtonLabel} onPress={() => this.openMainVideo("normal")}>
						<Image source={labelWatch} resizeMode={ImageResizeMode.contain} style={imgLabel} />
					</TouchableOpacity>
					<TouchableOpacity style={imgButtonLabel} onPress={() => this.openReadStory()}>
						<Image source={labelRead} resizeMode={ImageResizeMode.contain} style={imgLabel} />
					</TouchableOpacity>
				</View>
				<BottomLine />
			</ImageBackground>
		);
	}

	renderGrid() {
		const { wordVideo } = this.props;
		return (
			<FlatList
				data={wordVideo}
				style={styles.listContainer}
				renderItem={(item, index) => (<SubStoryCard item={item} index={index} showPreview={this.openFromCard} />)}
				numColumns={this.state.column}
			/>
		);
	}

	openMainVideo(type, subtitle) {
		const { mainVideo, title } = this.props;
		const { readVideo } = this.state;

		console.log('this.props', this.props);
		this.showPreview(mainVideo, readVideo && readVideo[0].subtitle_file);
		this.setState({
			normalUrl: mainVideo, title, isMainVideo: true, isReadVideo: true, paused: false, read: false, showNext: false,
			isSub: true, currentVideo: type, storyTitle: `Watching - ${readVideo && readVideo[0].video_title}`
		});
	}

	openFromCard(type, vocabularyUrl, storyTitle, index, subtitle) {
		this.setState({
			currentVideo: type, vocabularyUrl, isMainVideo: false,
			isReadVideo: false, isVocVideo: true, isSub: false, storyTitle: `Vocabulary - ${storyTitle}`, counter: index
		}, () => {
			this.showPreview(vocabularyUrl, subtitle)
		});
	}

	openReadStory() {
		const { readVideo } = this.state;
		this.showPreview(readVideo && readVideo[0].video_url, readVideo && readVideo[0].subtitle_file);
		this.setState({
			isSub: true, isReadVideo: true, paused: false, read: false, showNext: false,
			voc: false, currentVideo: "story", storyTitle: `Reading - ${readVideo && readVideo[0].video_title}`
		});
	}

	openVocaStory(index) {
		const { wordVideo } = this.props;
		this.showPreview(wordVideo[index].video_url, '');
		this.setState({
			paused: false, isVocVideo: true, read: false, voc: false,
			currentVideo: "voc", storyTitle: `Vocabulary - ${wordVideo[index].video_title}`
		});
	}

	showNext() {
		const { isReadVideo } = this.state;
		const { readVideo, wordVideo } = this.props;
		if (isReadVideo) {
			this.showPreview(readVideo[this.state.counter + 1].video_url, readVideo[this.state.counter + 1].subtitle_file)
			this.setState({ counter: this.state.counter + 1, showNext: false });
		} else {
			this.showPreview(wordVideo[this.state.counter + 1].video_url, '')
			this.setState({ counter: this.state.counter + 1, showNext: false, storyTitle: `Vocabulary - ${wordVideo[this.state.counter + 1].video_title}` });
		}
	}

	showPrevious() {
		const { isReadVideo } = this.state;
		const { readVideo, wordVideo } = this.props;
		if (isReadVideo) {
			this.setState({ counter: this.state.counter - 1, showNext: false });
			this.showPreview(readVideo[this.state.counter - 1].video_url, readVideo[this.state.counter - 1].subtitle_file)
		} else {
			this.setState({ counter: this.state.counter - 1, showNext: false, storyTitle: `Vocabulary - ${wordVideo[this.state.counter - 1].video_title}` });
			this.showPreview(wordVideo[this.state.counter - 1].video_url)
		}
	}

	showAgain() {
		const { currentVideo } = this.state;
		const { readVideo, wordVideo } = this.props;
		if (currentVideo == "normal") {
			if (Platform.OS == "ios") {
				this.player.seek(0);
			} else {
				// this.player.player.ref.seek(0);
			}
			this.setState({ paused: false, read: false, voc: false, isMainVideo: true });
		} else if (currentVideo == "story") {
			if (Platform.OS == "ios") {
				this.player.seek(0);
			} else {
				// this.player.player.ref.seek(0);
			}
			this.setState({ paused: false, read: false, voc: false, isReadVideo: true, counter: 0 });
			this.showPreview(readVideo && readVideo[0].video_url, readVideo && readVideo[0].subtitle_file);
		} else {
			if (Platform.OS == "ios") {
				this.player.seek(0);
			} else {
				// this.player.player.ref.seek(0);
			}
			this.setState({ paused: false, read: false, voc: false, isVocVideo: true, counter: 0 });
			this.showPreview(wordVideo && wordVideo[0].video_url)
		}
	}

	showPreview(previewUrl, subtitle) {
		this.setState({ previewUrl, isModelVideo: true, paused: false, subtitle });
	}

	onLoad(data) {
		this.setState({ duration: data.duration });
	}

	onProgress(data) {
		if (this.state.videoMaxTime != data.seekableDuration) {
			this.setState({
				videoMaxTime: data.seekableDuration
			})
			if (data.currentTime === data.duration) {
				this.onEnd();
			}
		}
		this.setState({
			progressVideoValue: data.currentTime,
			progressSliderValue: data.currentTime
		})
	}
	hideControls() {
		setTimeout(() => {
			this.setState({ hideControls: true })
		}, 3000)
	}

	onBuffer({ isBuffering }) {
		this.setState({ isBuffering });
	}

	onEnd = () => {
		const { isMainVideo, isReadVideo } = this.state;
		if (isMainVideo) {
			this.setState({ isMainVideo: false, read: true, voc: false });
		} else if (isReadVideo) {
			if (this.state.counter === (this.props.readVideo.length - 1)) {
				this.setState({ isReadVideo: false, read: false, voc: true, showNext: false, counter: 0 });
			} else {
				this.setState({ showNext: true });
			}
		} else {
			if (this.state.counter === (this.props.wordVideo.length - 1)) {

				this.setState({ isModelVideo: false, isMainVideo: true, isVocVideo: false, read: false, voc: false, counter: 0 })
			} else {
				this.setState({ showNext: true });
			}
		}
		this.setState({ paused: true });
	};

	onAudioBecomingNoisy = () => {
		this.setState({ paused: true });
	};

	onAudioFocusChanged = (event) => {
		this.setState({ paused: !event.hasAudioFocus });
	};
	showNextStory() {
		if (wordVideo != wordVideo.length - 1) {
			this.setState({ wordVideo: wordVideo + 1 }); //<-- use setState instead of assignment to update
		}
		else {
			this.setState({ wordVideo: 0 }); //<-- use setState instead of assignment to update
		}
	}
	showPreviewVideo() {
		const { previewUrl, paused, read, voc, isVocVideo, showNext, counter, subtitle } = this.state;
		const { container, imgbtnreplay, imgbtnreadstory, outerViewLeft,
			outerViewRight, outerViewNext, outerViewPrevious, imgBtnBack, imgBtnNext } = styleInline;
		return (
			<Modal animationType="slide"
				visible={this.state.isModelVideo}
				supportedOrientations={['landscape']}
				onRequestClose={() => { this.handleBackButtonClick() }}>
				<View style={{ flex: 1 }}>
					{!this.state.fullScreen &&
						<View style={container}>
							{this.renderBackIcon()}
							<View style={{ flex: 1 }}>
								{this.renderTitle()}
							</View>
							{!isVocVideo && this.renderSubtitle()}
						</View>
					}
					{
						Platform.OS == "ios" ?
							<Video source={{ uri: previewUrl }}
								ref={(ref) => {
									this.player = ref
								}}
								paused={paused}
								onLoad={this.onLoad}
								onBuffer={this.onBuffer}
								onProgress={this.onProgress}
								onPlaybackResume={() => { this.setState({ showNext: false }) }}
								onEnd={this.onEnd}
								textTracks={[
									{
										language: 'en',
										title: '#3 English Director Commentary',
										type: 'text/vtt',
										index: 2,
										uri: subtitle,
									}
								]}
								selectedTextTrack={{
									type: "index",
									value: 0
								}}
								controls
								onAudioBecomingNoisy={this.onAudioBecomingNoisy}
								onAudioFocusChanged={this.onAudioFocusChanged}
								style={styles.backgroundVideo}>
							</Video> :
							<TouchableOpacity
								onPress={() => { this.setState({ hideControls: !this.state.hideControls }); this.hideControls(); }}>
								<Video source={{ uri: previewUrl }}   // Can be a URL or a local file.
									ref={(ref) => {
										this.video = ref
									}}

									textTracks={[
										{
											language: 'en',
											title: '#3 English Director Commentary',
											type: 'text/vtt',
											index: 2,
											uri: subtitle,
										}
									]}

									selectedTextTrack={{
										type: "index",
										value: 0
									}}
									selectedAudioTrack={{
										type: "index",
										value: 0
									}}
									rate={this.state.rate}
									paused={this.state.paused}
									volume={this.state.volume}
									muted={this.state.muted}
									onPlaybackResume={() => { this.setState({ showNext: false }) }}
									ignoreSilentSwitch={this.state.ignoreSilentSwitch}
									resizeMode={this.state.resizeMode}
									onLoad={this.onLoad}
									onBuffer={this.onBuffer}
									onProgress={this.onProgress}
									repeat={true}
									onAudioBecomingNoisy={this.onAudioBecomingNoisy}
									onAudioFocusChanged={this.onAudioFocusChanged}
									onEnd={this.onEnd}

									style={{ width: width, height: this.state.fullScreen ? height - 24 : height - 50 }} />
							</TouchableOpacity>
					}
					{!this.state.hideControls &&
						<View style={{ position: 'absolute', width: width, height: height - 100, marginTop: 50 }}>
							<View style={{ height: height - 110, }}>
								<View style={{ backgroundColor: 'rgba(0,0,0,0.5)', width: 30, height: 30, marginHorizontal: 20, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
									<TouchableOpacity
										onPress={() => this.setState({ fullScreen: !this.state.fullScreen })}
										style={{ width: 50, height: 30, justifyContent: 'center', alignItems: 'center' }}
									>
										{
											!this.state.fullScreen ?
												<Entypo name='resize-full-screen' size={20} color={'#fff'} />
												:
												<Entypo name='resize-100-' size={20} color={'#fff'} />
										}
									</TouchableOpacity>
								</View>
							</View>
							<View style={{ flexDirection: 'row', backgroundColor: 'rgba(0,0,0,0.5)', height: 30, marginHorizontal: 20, borderRadius: 10 }}>
								<TouchableOpacity
									onPress={() => { this.setState({ paused: !this.state.paused }) }}
									style={{ width: 50, height: 30, justifyContent: 'center', alignItems: 'center' }}
								>
									{
										this.state.paused ?
											<AntDesign name='play' size={20} color={'#fff'} />
											:
											<AntDesign name='pause' size={20} color={'#fff'} />
									}
								</TouchableOpacity>
								<View
									style={{ width: width - 105, height: 30, justifyContent: 'center' }}
								>
									<Slider
										value={this.state.progressSliderValue}
										//  onValueChange={(data) => this.jumpToVideoTime(data)}
										maximumTrackTintColor='#c9c5bf'
										minimumTrackTintColor='#e11a21'
										thumbTintColor='#e11a21'
										maximumValue={this.state.videoMaxTime}
										thumbStyle={{ height: 12, width: 12 }}
									/>
								</View>
							</View>
						</View>
					}
					{read &&
						<TouchableOpacity style={outerViewLeft} onPress={() => this.showAgain()}>
							<Image
								source={btnreplay}
								style={imgbtnreplay}
								resizeMode={ImageResizeMode.contain} />
						</TouchableOpacity>
					}
					{read &&
						<TouchableOpacity style={outerViewRight} onPress={() => this.openReadStory()}>
							<Image source={btnreadstory} style={imgbtnreadstory} resizeMode={ImageResizeMode.contain} />
						</TouchableOpacity>
					}
					{
						showNext && counter !== 0 &&
						<TouchableOpacity style={outerViewPrevious} onPress={() => this.showPrevious()}>
							<Image source={btnbackward} style={imgBtnBack} resizeMode={ImageResizeMode.contain} />
						</TouchableOpacity>
					}
					{voc &&
						<TouchableOpacity style={outerViewLeft} onPress={() => this.showAgain()}>
							<Image source={btnreadagain} style={imgbtnreplay} resizeMode={ImageResizeMode.contain} />
						</TouchableOpacity>
					}
					{showNext && <TouchableOpacity style={outerViewNext} onPress={() => this.showNext()}>
						<Image source={btnforward} style={imgBtnNext} resizeMode={ImageResizeMode.contain} />
					</TouchableOpacity>}
					{voc &&
						<TouchableOpacity style={outerViewRight} onPress={() => this.openVocaStory(0)}>
							<Image source={btnvocabulary} style={imgbtnreadstory} resizeMode={ImageResizeMode.contain} />
						</TouchableOpacity>
					}
				</View>
			</Modal>
		)
	}

	renderBackIcon() {
		const { iconLeft } = styleInline;
		return (
			<TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.handleBackButtonClick()}>
				<Icon name='chevron-thin-left' type='Entypo' style={iconLeft} />
			</TouchableOpacity>
		)
	}

	renderTitle() {
		return <Text style={[styleInline.txtTitle]}>{this.state.storyTitle}</Text>
	}

	renderSubtitle() {
		return (
			<TouchableOpacity disabled onPress={() => { }}>
				<Image source={this.state.isSub ? subtitlefill : subtitle} style={[styleInline.iconRight, { tintColor: '#FFF' }]} />
			</TouchableOpacity>
		)
	}

	render() {
		const { container, containerPortrait, backgroundImage, bottomContainer } = styles;
		const { title } = this.props
		return (
			<View style={[(this.state.orientation == 'portrait') ? containerPortrait : container, { backgroundColor: purple }]} ref="rootView">
				<Appbar icon={false} title={title} color="#72b45a" />
				<ScrollView
					overScrollMode="auto"
					style={{ flex: 1 }} contentContainerStyle={{ alignItems: 'center', backgroundColor: purple }}>
					{this.renderBannerView()}
					<ImageBackground source={starbackground} style={backgroundImage}>
						<Image source={words} style={{ alignSelf: 'center', marginTop: -40, width: 300 }} resizeMode={ImageResizeMode.contain} />
					</ImageBackground>

					<View style={{ backgroundColor: purple, flex: 1 }}>
						{this.renderGrid()}
					</View>
					<View style={{ flex: 1, width: width, justifyContent: 'center', alignItems: 'center' }}>
						<ImageBackground source={bottomfooter} style={{ width: width - 50, height: 150, alignItems: 'center' }} resizeMode={ImageResizeMode.stretch}>
							<TouchableOpacity onPress={() => Actions.pop()}>
								<Image source={watchmore} resizeMode={ImageResizeMode.contain} style={{ height: 90, width: 180, marginTop: 15 }} />
							</TouchableOpacity>
						</ImageBackground>
					</View>
					<BottomLine />
				</ScrollView>
				{this.showPreviewVideo()}
			</View>
		);
	}
}

const styleInline = {
	container: {
		flexDirection: 'row',
		height: (Platform.OS === 'ios') ? 60 : 50,
		backgroundColor: purple,
		paddingTop: (Platform.OS === 'ios') ? 10 : 0
	},
	iconLeft: {
		color: "#FFF",
		marginTop: 10,
		marginLeft: 10,
	},
	iconRight: {
		marginRight: 30,
		marginTop: 10,
		height: 35,
		width: 35
	},
	txtTitle: {
		fontSize: 20,
		fontWeight: 'bold',
		marginTop: 15,
		color: '#FFF',
		alignSelf: 'center'
	},
	imgbtnreplay: {
		height: 150,
		width: 150,
	},
	imgbtnreadstory: {
		height: 150,
		width: 150,
	},
	imgBtnBack: {
		height: 80,
		width: 80,
	},
	imgBtnNext: {
		height: 80,
		width: 80,
	},
	outerViewPrevious: {
		top: 120,
		left: -40,
		position: 'absolute'
	},
	outerViewCenter: {
		top: 120,
		right: '30%',
		position: 'absolute'
	},
	outerViewNext: {
		top: 120,
		right: -40,
		position: 'absolute'
	},
	outerView: {
		flex: 1,
		position: 'absolute',
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	outerViewLeft: {
		top: 120,
		left: '25%',
		position: 'absolute'
	},
	outerViewRight: {
		top: 120,
		right: '25%',
		position: 'absolute'
	}
}

export default Story;

