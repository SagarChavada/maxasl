import React, { Component } from 'react'
import { View, ImageBackground, Dimensions, ScrollView, StatusBar, 
    TouchableOpacity, Image, BackHandler, Platform, Text, ToastAndroid } from 'react-native';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode';
import Toast from 'react-native-toast-native';
import { connect } from 'react-redux';
import styles from '../styles';
import { starbackground, purple, arrowLeft, arrowRight } from '../helper/constants';
import Appbar from '../helper/appbar';
import StoryCard from '../helper/storycard/favoriteStorycard';
import { fetchCategory } from '../action';
import BottomLine from '../helper/bottomline';

const { height, width } = Dimensions.get('window');
const aspectRatio = height / width;

const style={
    backgroundColor: "#000000",
    width: 300,
    height: 50,
    fontSize: 18,
    borderRadius: 15,
};

let data = [];

class Dashboard extends Component {

    constructor() {
        super();
        this.onNextClick = this.onNextClick.bind(this)
        this.onPreviosClick = this.onPreviosClick.bind(this)
        this.renderCardlistIpadSmall = this.renderCardlistIpadSmall.bind(this);
        this.renderCardlistIpadBig = this.renderCardlistIpadBig.bind(this);
        this.showToast = this.showToast.bind(this);
        this.getStory = this.getStory.bind(this);
    }

    state = { 
        orientation: '',
        stories: [],
        count: 0, 
        isScroll: false,
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentDidMount() {
        this.getOrientation();
        Dimensions.addEventListener('change', () => {
            this.getOrientation();
        });
        this.getStory();
    }

    getStory() {
        const stories = this.props.stories.data;
        stories.map((item, index) => {
            item.favourite && data.push(item);
        }) 
        this.setState({ stories: data})
    }

    componentDidUpdate(prevProps) {
        data = [];
        if(prevProps.stories != this.props.stories) {
            this.getStory();
        }
    }

    componentWillUnmount() {
        data = [];
    }

    onNextClick() {
        this.setState({count: this.state.count - 1}, () => {
                this.scroll.scrollTo({x: this.state.count * 230, y: 0, animated: true})
            } 
        );
    }

    onPreviosClick() {
        this.setState({count: this.state.count + 1}, () => {
            this.scroll.scrollTo({x: this.state.count * 230, y: 0, animated: true})
        });
    }

    showToast(title) {
        if(Platform.OS == "ios") {
            Toast.show(title,Toast.SHORT, Toast.TOP, style);
        } else {
            ToastAndroid.showWithGravity(
                title,
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
              );
        }
        this.props.fetchCategory();
    }

    
    renderTitle() {
        return <Text style={[styleInline.txtTitle]}>{this.state.storyTitle}</Text>
    }

    videoError = (error) => {
    }

    getOrientation = () => {
        if (this.refs.rootView) {
            if (Dimensions.get('window').width < Dimensions.get('window').height) {
                this.setState({ orientation: 'portrait' });
            }
            else {
                this.setState({ orientation: 'landscape' });
            }
        }
    }

    renderView() {
        const {  imgBackground } = styles;
        return(
            <ImageBackground source={starbackground} style={imgBackground} resizeMode={ImageResizeMode.contain}>
                <ScrollView ref = {re => this.scroll = re} horizontal={true} showsHorizontalScrollIndicator={false}>
                    <View style={{ flex: 1, justifyContent: 'flex-end', marginLeft: 15 }}>
                        {aspectRatio > 1.6 ?
                            this.renderCardlistIphone() :
                            aspectRatio > 0.66 ? this.renderCardlistIpadSmall() : this.renderCardlistIpadBig()}
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }

    render() {
        const { container, containerPortrait } = styles;
        return (
            <View style={[(this.state.orientation == 'portrait') ? containerPortrait : container, { backgroundColor: purple }]} ref="rootView">
                <StatusBar
                    backgroundColor="white"
                    barStyle="dark-content"
                />
                <Appbar icon={false}  isFav={true}/>
                { 
                    this.state.stories.length > 0 ?
                    this.renderView() : this.renderNoFavorite()
                }
                <BottomLine />
                {this.state.stories.length > 3 && this.renderLeftArrow()}
                {this.state.stories.length > 3 && this.renderRightArrow()}
            </View>
        )
    };

    renderNoFavorite() {
        return(
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{ color: '#FFF', fontSize: 25 }}>No Favorite stories yet.</Text>
            </View>
        )
    }

    renderCardlistIphone() {
        const {stories, count} = this.state;
        return (
            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                {
                    stories.map((item, index) => {
                        return index == count ? <StoryCard showToast={this.showToast} showPreview={this.showPreview} height={260} items={item} key={index} imgHeight={190} width={210} innerWidth={190} /> 
                        : <StoryCard showToast={this.showToast} showPreview={this.showPreview} height={240} key={index} items={item} imgHeight={170} width={210} innerWidth={190} />
                    })
                }
            </View>
        )
    }

    renderLeftArrow() {
        return (
            <View style={styles.footerLeftErroView} >
                <TouchableOpacity onPress={() => this.state.count != 0 && this.onNextClick()}>
                    <Image style={styles.footerErroImage} source={arrowLeft} />
                </TouchableOpacity>
            </View>
        )
    }

    renderRightArrow() {
        return (
            <View style={styles.footerRightErroView}>
                <TouchableOpacity onPress={() => this.state.count != this.state.mainArray.length - 4 && this.onPreviosClick()}>
                    <Image style={styles.footerErroImage} source={arrowRight} />
                </TouchableOpacity>
            </View>
        )
    }

    renderCardlistIpadSmall() {
        const {stories, count} = this.state;
        return (
            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                {
                    stories.map((item, index) => {
                        return index == count ? <StoryCard showToast={this.showToast} showPreview={this.showPreview} items={item} height={220} key={index} imgHeight={150} width={200} innerWidth={190} />
                         : <StoryCard showToast={this.showToast} showPreview={this.showPreview} items={item} height={200} key={index} imgHeight={130} width={200} innerWidth={190} />
                    })
                }
            </View>
        )
    }

    renderCardlistIpadBig() {
        const {stories, count} = this.state;
        return (
                <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                {
                    stories.map((item, index) => {
                        return index == count ? <StoryCard showToast={this.showToast} showPreview={this.showPreview} height={270} items={item} key={index} imgHeight={200} width={210} innerWidth={190} /> 
                        : <StoryCard showToast={this.showToast} showPreview={this.showPreview} height={250} key={index} imgHeight={180} items={item} width={200} innerWidth={190} />
                    })
                }
                </View>    
        )
    }
}

const styleInline = {
    container: {
        flexDirection: 'row',
        height: ( Platform.OS === 'ios' ) ? 60 : 50,
        backgroundColor: purple,
        paddingTop: ( Platform.OS === 'ios' ) ? 10 : 0
    },
    iconLeft: {
        color: "#FFF",
        marginTop: 10,
        marginLeft: 10,
    },
    iconRight: {
        marginRight: 30,
        marginTop: 10,
        height: 35,
        width: 35
    },
    txtTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 15,
        color: '#FFF',
        alignSelf: 'center',
    },
    imgbtnreplay: {
        height: 150,
        width: 150,
        top: 120,
        left: '25%',
        position: 'absolute'
    },
    imgbtnreadstory: {
        height: 150,
        width: 150,
        top: 120,
        right: '25%' ,
        position: 'absolute'
    }
}

function mapStateToProps({ stories }) {
	return { stories };
}
export default connect(mapStateToProps, { fetchCategory })(Dashboard);