import { FETCH_ITEM } from '../action';

// Reducer for fetching Current User
export default function (state = null, actions) {
	switch (actions.type) {
		case FETCH_ITEM:
			return actions.payload || false;	
		default:
			return state;
	}
}
