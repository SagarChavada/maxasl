import { FETCH_CATEGORY } from '../action';

// Reducer for fetching Current User
export default function (state = null, actions) {
	switch (actions.type) {
		case FETCH_CATEGORY:
			return actions.payload.data || false;	
		default:
			return state;
	}
}
