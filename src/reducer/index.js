import { combineReducers } from 'redux';
import storyReducer from './storyReducer';
import fileReducer from './fileReducer';
import subScribeReducer from './subscribeReducer';
import allReducer from './allReducer';
import purchaseReducer from './purchaseReducer';

export default combineReducers({
	stories: storyReducer,
	files: fileReducer,
	subscription: subScribeReducer,
	fetchAll: allReducer,
	purchase: purchaseReducer,
});
