import { PURCHASE_USER_API } from '../action';

// Reducer for fetching Current User
export default function (state = null, actions) {
	switch (actions.type) {
		case PURCHASE_USER_API:
			return actions.payload || false;	
		default:
			return state;
	}
}
