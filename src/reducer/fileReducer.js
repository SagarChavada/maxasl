import { FETCH_FILES } from '../action';

// Reducer for fetching Current User
export default function (state = null, actions) {
	switch (actions.type) {
		case FETCH_FILES:
			return actions.payload || [];	
		default:
			return state;
	}
}
