/** @format */
import React from 'react';
import {AppRegistry} from 'react-native';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import logger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import promise from 'redux-promise-middleware';
import App from './App';
import {name as appName} from './app.json';

import reducers from './src/reducer';

const axiosInstance = axios.create({
	baseURL: ''
});

// Creating redux store
const store = createStore(
	reducers,
//	applyMiddleware(thunk.withExtraArgument(axiosInstance), promise(), logger)
	applyMiddleware(thunk.withExtraArgument(axiosInstance), promise())
);

const route = () => {
    return (
      <Provider store={store}>
          <App />
      </Provider>
    );
  };

AppRegistry.registerComponent(appName, () => route);
