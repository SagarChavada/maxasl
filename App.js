import React, {Component} from 'react';
import {YellowBox} from 'react-native'; 
import Navigator from './src/Navigator';
import SplashScreen from 'react-native-splash-screen'


export default class App extends Component {

  constructor(props) {
    super(props);
    console.disableYellowBox = true;
    YellowBox.ignoreWarnings([
     'Warning: Each',
     'Warning: Failed'
   ]);
  } 

  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return <Navigator />;
  }
}